package com.boarsoft.cache.redis;

import java.io.IOException;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.ServiceLoader;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.boarsoft.cache.config.CacheDb;
import com.boarsoft.cache.config.CacheShard;
import com.boarsoft.common.Util;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.exceptions.JedisException;
import redis.clients.jedis.util.Hashing;
import redis.clients.jedis.util.MurmurHash;

/**
 * 自定义缓存集群，包含多个分片，每个分片一主多备（主从/主备）
 * 
 * @author Mac_J
 *
 */
public class MyJedisDb implements CacheDb, Serializable {
	private static final long serialVersionUID = 4780279299748415786L;
	private static final Logger log = LoggerFactory.getLogger(MyJedisDb.class);

	/** */
	protected String code;
	/** */
	protected List<CacheShard> shards;
	/** */
	protected static Hashing hashing = new MurmurHash();
	/** */
	protected static String hashingName = hashing.getClass().getName();

	static {
		ServiceLoader<Hashing> sl = ServiceLoader.load(Hashing.class);
		Iterator<Hashing> it = sl.iterator();
		if (it.hasNext()) {
			hashing = it.next();
			hashingName = hashing.getClass().getName();
		}
	}

	@Override
	public void init() {
		if (shards == null || shards.isEmpty()) {
			throw new IllegalStateException(String.format("Shards of %s is required", code));
		}
		// 初始化所有分片的连接（与分片中主节点的连接）
		log.info("Init {} shards for MyJedisDb {}", shards.size(), code);
		for (int i = 0; i < shards.size(); i++) {
			CacheShard cs = shards.get(i);
			cs.init(code, i);
		}
	}

	@Override
	public Object getResource(String key) {
		return this.getResource(key, false);
	}

	@Override
	public Object getResource(String key, boolean readOnly) {
		int s = shards.size();
		if (s == 0) {
			throw new IllegalStateException(String.format("No available shards for %s", code));
		}
		int i = 0;
		if (s > 1 && Util.strIsNotEmpty(key)) {
			Long h = hashing.hash(key) % s;
			// int h = key.hashCode();
			i = Math.abs(h.intValue());
		}
		MyJedisShard cs = (MyJedisShard) shards.get(i);
		try {
			return (Jedis) cs.getResource(readOnly);
		} catch (Exception e) {
			throw new JedisException(String.format("Could not get jedis of %s/%s", //
					code, cs.getMaster().getCode()), e);
		}
	}

	@Override
	public Map<String, Object> getResourceMap() {
		Map<String, Object> rm = new HashMap<String, Object>();
		for (CacheShard cs : shards) {
			rm.put(cs.getDbCode(), cs.getResource());
		}
		return rm;
	}

	@Override
	public void close() {
		for (CacheShard cs : shards) {
			try {
				cs.close();
			} catch (IOException e) {
				log.error("Error on close cache shard {}", cs, e);
			}
		}
	}

	@Override
	public CacheShard getShard(int index) {
		if (index < shards.size()) {
			return shards.get(index);
		}
		log.error("Cache shard index {} for cache db {} is out of range", index, this);
		return null;
	}

	@Override
	public List<CacheShard> getShards() {
		return shards;
	}

	public void setShards(List<CacheShard> shards) {
		this.shards = shards;
	}

	public void setCode(String code) {
		this.code = code;
	}

	@Override
	public String getCode() {
		return code;
	}
}
