package com.boarsoft.cache.redis;

import java.io.File;

import com.boarsoft.bean.ReplyInfo;

public interface RedisNodeAgent {
	/**
	 * 
	 * @param ip
	 * @param port
	 * @return
	 */
	ReplyInfo<Object> startup(String ip, int port);

	/**
	 * 
	 * @param ip
	 * @param port
	 * @param master
	 * @return
	 */
	ReplyInfo<Object> toSlave(String ip, int port, String master);

	/**
	 * 
	 * @param ip
	 * @param port
	 * @return
	 */
	ReplyInfo<Object> shutdown(String ip, int port);

	/**
	 * 
	 * @param ip
	 * @param port
	 * @return
	 */
	ReplyInfo<Object> toMaster(String ip, int port);

	/**
	 * 
	 * @param ip
	 * @param port
	 * @param master
	 * @return
	 */
	ReplyInfo<Object> startup(String ip, int port, String master);

	/**
	 * 获取redis节点的信息
	 * 
	 * @param host
	 * @param port
	 * @return
	 */
	ReplyInfo<Object> getInfo(String host, int port);

	/**
	 * 执行手工输入的命令
	 * 
	 * @param host
	 *            redis host
	 * @param port
	 *            redis port
	 * @param cmd
	 * @return
	 */
	ReplyInfo<Object> exec(String host, int port, String cmd);

	/**
	 * 批量执行命令行或导入
	 * 
	 * @param file
	 * @param host
	 *            redis host
	 * @param port
	 *            redis port
	 * @param to
	 * @param cmd
	 *            batch  : 批量执行redis命令，同： cat cmds.txt | redis-cli -p 6379 <br>
	 *            dumpin : 批量导入，同 redis-cli -p 6379 -x set key1 < data.txt
	 * @return
	 */
	ReplyInfo<Object> batch(File file, String host, int port, String to, String cmd);
}
