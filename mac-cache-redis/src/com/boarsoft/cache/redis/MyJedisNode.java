package com.boarsoft.cache.redis;

import java.io.Serializable;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.boarsoft.bean.ReplyInfo;
import com.boarsoft.cache.config.CacheNode;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;
import redis.clients.jedis.exceptions.JedisConnectionException;

/**
 * 一个shard有多个node，其中只有一个主节点，N个逻辑上的备用节点
 * 
 * @author Mac_J
 *
 */
public class MyJedisNode implements CacheNode, Serializable {
	private static final long serialVersionUID = 3648401079301002160L;

	private static final Logger log = LoggerFactory.getLogger(MyJedisNode.class);

	protected MyJedisShardInfo jedisShardInfo;
	protected String code;
	protected short status = CacheNode.STATUS_DOWN;
	/** */
	protected MyJedisPoolConfig myJedisPoolConfig;

	/** */
	@Autowired(required = false)
	protected transient RedisNodeAgent redisNodeAgent;
	/** */
	protected transient JedisPool jedisPool;

	@Override
	public Object getResource() {
		this.open();
		log.debug("Get resource from jedis pool {}/{}/{}/{}/{}", //
				myJedisPoolConfig.getMaxTotal(), myJedisPoolConfig.getMaxIdle(), //
				jedisPool.getNumActive(), jedisPool.getNumIdle(), jedisPool.getNumWaiters());
		return jedisPool.getResource();
	}

	@Override
	public void open() {
		if (jedisPool != null) {
			return;
		}
		log.warn("Connect MyJedisNode {}", code);
		if (myJedisPoolConfig == null) {
			myJedisPoolConfig = new MyJedisPoolConfig();
		}
		JedisPoolConfig jpc = new JedisPoolConfig();
		jpc.setMaxIdle(myJedisPoolConfig.getMaxIdle());
		jpc.setMaxTotal(myJedisPoolConfig.getMaxTotal());
		jpc.setMinIdle(myJedisPoolConfig.getMinIdle());
		this.jedisPool = new JedisPool(jpc, //
				jedisShardInfo.getHost(), jedisShardInfo.getPort(),
				jedisShardInfo.getConnectionTimeout(), jedisShardInfo.getSoTimeout(),
				jedisShardInfo.getPassword(), jedisShardInfo.getDb(),
				jedisShardInfo.getName(), jedisShardInfo.getSsl(),
				jedisShardInfo.getSslSocketFactory(),jedisShardInfo.getSslParameters(),
				jedisShardInfo.getHostnameVerifier());
	}

	@Override
	public void close() {
		if (jedisPool != null) {
			jedisPool.close();
			jedisPool = null;
		}
	}

	@Override
	public boolean equals(Object o) {
		if (o == null || !(o instanceof MyJedisNode)) {
			return false;
		}
		MyJedisNode mjsi = (MyJedisNode) o;
		return this.code.equals(mjsi.getCode());
	}

	@Override
	public int hashCode() {
		return code.hashCode();
	}

	@Override
	public String getHost() {
		return jedisShardInfo.getHost();
	}

	@Override
	public int getPort() {
		return jedisShardInfo.getPort();
	}

	@Override
	public String toString() {
		return code;
	}

	@Override
	public boolean isReadonly() {
		return status == CacheNode.STATUS_READ;
	}

	@Override
	public boolean isReadable() {
		return status >= CacheNode.STATUS_READ;
	}

	@Override
	public boolean isWriteable() {
		return status == CacheNode.STATUS_WRITE;
	}

	@Override
	public short test() {
		// Assert.notNull(jedisShardInfo);
		String h = jedisShardInfo.getHost();
		int p = jedisShardInfo.getPort();
		Jedis jedis = null;
		for (int i = 0; i < myJedisPoolConfig.testTimes; i++) {
			try {
				this.open();
				jedis = this.jedisPool.getResource();
				if (jedis == null) {
					status = CacheNode.STATUS_DOWN;
				} else {
					jedis.get("_test");
					status = CacheNode.STATUS_READ; // 可读
					try {
						jedis.set("_test", code);
						status = CacheNode.STATUS_WRITE; // 可写
					} catch (Exception e) {
						// log.warn("Cache node {} is readonly", this);
					}
				}
				return status;
			} catch (Exception e) {
				if (e instanceof JedisConnectionException) {
					log.warn("Can not connect cache node {}({}:{})", code, h, p);
				} else {
					log.error("Error on test cache node {}({}:{})", code, h, p, e);
				}
				try {
					Thread.sleep(myJedisPoolConfig.testInterval);
				} catch (InterruptedException e1) {
					log.error("Be interrupted while sleep for next test");
				}
			} finally {
				if (jedis != null) {
					jedis.close();
				}
			}
		}
		status = CacheNode.STATUS_DOWN;
		return status;
	}

	/**
	 * 启动指定节点，如果该节点还未启动的话
	 * 
	 * @param csi
	 * @return
	 */
	@Override
	public boolean startUp() {
		if (redisNodeAgent == null) {
			log.warn("Redis node agent of {} is required", code);
			return false;
		}
		if (this.isReadable()) {
			log.warn("Cache shard {} already be started up.", this);
			return true;
		}
		log.warn("Start up cache node {}", this);
		ReplyInfo<Object> ri = redisNodeAgent.startup(//
				this.getHost(), this.getPort());
		if (ri.isSuccess()) {
			log.info("Startup cache node {} sucessfully.", this);
		} else {
			log.error("Failed to startup cache node {}. {}", this, ri.getData());
		}
		return ri.isSuccess();
	}

	@Override
	public boolean shutdown() {
		if (redisNodeAgent == null) {
			log.warn("Redis node agent of {} is required", code);
			return false;
		}
		log.warn("Shutdown cache node {}", this);
		ReplyInfo<Object> ri = redisNodeAgent.shutdown(//
				this.getHost(), this.getPort());
		if (ri.isSuccess()) {
			log.info("Shutdown cache node {} sucessfully.", this);
		} else {
			log.error("Failed to shutdown cache node {}. {}", this, ri.getData());
		}
		return ri.isSuccess();
	}

	@Override
	public boolean toMaster() {
		if (redisNodeAgent == null) {
			log.warn("Redis node agent of {} is required", code);
			return false;
		}
		log.warn("Switch cache node {} to master", this);
		ReplyInfo<Object> ri = redisNodeAgent.toMaster(//
				this.getHost(), this.getPort());
		if (ri.isSuccess()) {
			log.info("Switch cache node {} to master successfully.", this);
		} else {
			log.error("Failed to switch cache node {} to master. {}", this, ri.getData());
		}
		return ri.isSuccess();
	}

	@Override
	public boolean toSlave(String master) {
		if (redisNodeAgent == null) {
			log.warn("Redis node agent of {} is required", code);
			return false;
		}
		log.warn("Switch cache node {} to slave", this);
		ReplyInfo<Object> ri = redisNodeAgent.toSlave(//
				this.getHost(), this.getPort(), master);
		if (ri.isSuccess()) {
			log.info("Switch cache node {} to slave sucessfully.", this);
		} else {
			log.error("Failed to switch cache node {} to slave. {}", this, ri.getData());
		}
		return ri.isSuccess();
	}

	@Override
	public void clearAll() {
		// Assert.notNull(jedisShardInfo);
		String h = jedisShardInfo.getHost();
		int p = jedisShardInfo.getPort();
		Jedis jedis = null;
		try {
			jedis = new Jedis(h, p);
			Set<String> keys = jedis.keys("*");
			int size = keys.size();
			if (size > 0) {
				log.warn("Gona del {} keys at once", size);
				String[] ka = new String[size];
				keys.toArray(ka);
				jedis.del(ka);
			}
		} catch (Exception e) {
			log.error("Error on clear cache node {}:{}", h, p, e);
		} finally {
			jedis.close();
		}
	}

	public void setJedisPoolConfig(JedisPoolConfig jedisPoolConfig) {
		this.myJedisPoolConfig = new MyJedisPoolConfig();
		this.myJedisPoolConfig.setMaxIdle(jedisPoolConfig.getMaxIdle());
		this.myJedisPoolConfig.setMaxTotal(jedisPoolConfig.getMaxTotal());
		this.myJedisPoolConfig.setMinIdle(jedisPoolConfig.getMinIdle());
	}

	public MyJedisPoolConfig getJedisPoolConfig() {
		return this.myJedisPoolConfig;
	}

	public MyJedisShardInfo getJedisShardInfo() {
		return jedisShardInfo;
	}

	public void setJedisShardInfo(MyJedisShardInfo jedisShardInfo) {
		this.jedisShardInfo = jedisShardInfo;
	}

	@Override
	public String getCode() {
		return code;
	}

	@Override
	public short getStatus() {
		return status;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public void setStatus(short status) {
		this.status = status;
	}

	public JedisPool getJedisPool() {
		return jedisPool;
	}

	public void setJedisPool(JedisPool jedisPool) {
		this.jedisPool = jedisPool;
	}

	public MyJedisPoolConfig getMyJedisPoolConfig() {
		return myJedisPoolConfig;
	}

	public void setMyJedisPoolConfig(MyJedisPoolConfig myJedisPoolConfig) {
		this.myJedisPoolConfig = myJedisPoolConfig;
	}
}
