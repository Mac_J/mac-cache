package com.boarsoft.cache.redis;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.boarsoft.cache.Cache;
import com.boarsoft.cache.combo.ComboCache;
import com.boarsoft.common.Util;

/**
 * 实现本地缓存（JVMM或DM）与远程REDIS缓存的联动 <br>
 * 统一以字符串为key，redis 中 value为二进制字节数组（Hessian序列化），本地缓存中为对象<br>
 * jvmm 中未命中则到 redis 获取 <br>
 * 写入 jvmm 时同步写入到 redis <br>
 * 实现 jvmm 与 redis 之间的数据格式转换
 * 
 * @author Mac_J
 *
 */
public class SimpleJvmmRedis implements ComboCache {
	private static final Logger log = LoggerFactory.getLogger(SimpleJvmmRedis.class);

	/** cache1 */
	protected Cache cache1; // JVMM
	/** cache2 */
	protected Cache cache2; // REDIS
	/** 初始化时，需要装载的组 */
	protected Map<String, String> loadMap = new HashMap<String, String>();
	/** */
	protected boolean required = false;

	@PostConstruct
	public void init() throws Exception {
		try {
			this.load();
		} catch (Exception e) {
			if (required) {
				throw e;
			}
			log.warn("Ignored exception on init", e);
		}
	}

	@Override
	public void load() throws Exception {
		log.info("Load data from cache2 to cache1");
		cache1.clear();
		Map<String, Set<String>> dm = cache2.getDbMaping();
		if (dm == null) {
			log.warn("Cache2 has no db mapping.");
			return;
		}
		// d = dbName, 从远程缓存载入数据到当前缓存中
		for (Entry<String, Set<String>> en : dm.entrySet()) {
			String d = en.getKey();
			if (!loadMap.containsKey(d)) {
				continue;
			}
			// groups
			String gs = loadMap.get(d);
			String[] ga = gs.split(",");
			Set<String> set = en.getValue();
			log.debug("Read cache2 {}, {} groups", d, set.size());
			for (String g : set) {
				if (Util.strIsEmpty(gs)) {
					continue;
				}
				if (!"*".equals(gs)) {
					if (Util.inArray(g, ga) < 0) {
						continue;
					}
				}
				Map<String, Object> m = cache2.list(d, g);
				log.debug("Read cache2 pool/group {}/{}, {} keys", d, g, m.size());
				for (Entry<String, Object> en2 : m.entrySet()) {
					String k = en2.getKey();
					String s = k.substring(g.length() + 1);
					log.debug("Read cache2 pool/group/key {}/{}/{} = {}", d, g, s, m.get(k));
					cache1.put(g, s, m.get(k));
				}
			}
		}
	}

	@Override
	public void put(String group, String key, Object value) throws Exception {
		cache1.put(group, key, value);
	}

	@Override
	public Object get(String group, String key) throws Exception {
		return cache1.get(group, key);
	}

	@Override
	public void remove(String group) {
		cache1.remove(group);
	}

	@Override
	public void remove(String group, String key) throws UnsupportedEncodingException {
		cache1.remove(group, key);
	}

	@Override
	public Map<String, Object> list(String group) throws Exception {
		return cache1.list(group);
	}

	@Override
	public long incr(String group, String key, long delta) {
		return cache1.incr(group, key, delta);
	}

	@Override
	public void set(String group, String key, Map<String, Object> om) throws Exception {
		cache1.set(group, key, om);
	}

	@Override
	public void set2(String group, String key, Map<String, Object> om) throws Exception {
		cache1.set(group, key, om);
		cache2.set(group, key, om);
	}

	@Override
	public void get(String group, String key, Map<String, Object> om) throws Exception {
		cache1.get(group, key, om);
	}

	@Override
	public void get2(String group, String key, Map<String, Object> om) throws Exception {
		if (cache1.exists(group, key)) {
			cache1.get(group, key, om);
			return;
		}
		cache2.get(group, key, om);
		cache1.set(group, key, om);
	}

	@Override
	public void put2(String group, String key, Object value) throws Exception {
		cache1.put(group, key, value);
		cache2.put(group, key, value);
	}

	@Override
	public Object get2(String group, String key) throws Exception {
		Object o = cache1.get(group, key);
		if (o == null) {
			o = cache2.get(group, key);
			if (o != null) {
				cache1.put(group, key, o);
			}
		}
		return o;
	}

	@Override
	public void clear2(String group) {
		cache1.remove(group);
		cache2.remove(group);
	}

	@Override
	public void clear() {
		cache1.clear();
	}

	@Override
	public void clear2() {
		cache1.clear();
		cache2.clear();
	}

	@Override
	public void remove2(String group) {
		cache1.remove(group);
		cache2.remove(group);
	}

	@Override
	public void remove2(String group, String key) throws UnsupportedEncodingException {
		cache1.remove(group, key);
		cache2.remove(group, key);
	}

	@Override
	public long incr2(String group, String key, long delta) throws Exception {
		// long v = cache2.incr(group, key, delta);
		// cache1.put(group, key, v);
		cache2.incr(group, key, delta);
		return cache1.incr(group, key, delta);
	}

	@Override
	public boolean exists(String group, String key) throws UnsupportedEncodingException {
		return cache1.exists(group, key);
	}

	@Override
	public boolean exists2(String group, String key) throws UnsupportedEncodingException {
		boolean b = cache1.exists(group, key);
		return b || cache2.exists(group, key);
	}

	@Override
	public Cache getCache1() {
		return cache1;
	}

	public void setCache1(Cache cache1) {
		this.cache1 = cache1;
	}

	@Override
	public Cache getCache2() {
		return cache2;
	}

	public void setCache2(Cache cache2) {
		this.cache2 = cache2;
	}

	public Map<String, String> getLoadMap() {
		return loadMap;
	}

	public void setLoadMap(Map<String, String> loadMap) {
		this.loadMap = loadMap;
	}

	public boolean isRequired() {
		return required;
	}

	public void setRequired(boolean required) {
		this.required = required;
	}

	@Override
	public void put(String group, String key, Object value, int expire) throws Exception {
		cache1.put(group, key, value, expire);
		cache2.put(group, key, value, expire);
	}

	@Override
	public void set(String group, String key, Map<String, Object> om, int expire) throws Exception {
		cache1.set(group, key, om, expire);
		cache2.set(group, key, om, expire);
	}

	@Override
	public Map<String, Set<String>> getDbMaping() {
		Map<String, Set<String>> rm = cache1.getDbMaping();
		if (rm == null) {
			rm = cache2.getDbMaping();
		}
		return rm;
	}

	@Override
	public Map<String, Object> list(String dbName, String group) throws Exception {
		Map<String, Object> rm = cache1.list(dbName, group);
		if (rm == null || rm.isEmpty()) {
			rm = cache2.list(dbName, group);
		}
		return rm;
	}

	@Override
	public String str(String group, String key) {
		return cache1.str(group, key);
	}

	@Override
	public void str(String group, String key, String value, int expire) {
		cache1.str(group, key, value, expire);
	}

	@Override
	public void str(String group, String key, String value) {
		cache1.str(group, key, value);
	}

	@Override
	public String str2(String group, String key, int expire) {
		String o = cache1.str(group, key);
		if (o == null) {
			o = cache2.str(group, key);
			if (o != null) {
				cache1.str(group, key, o, expire);
			}
		}
		return o;
	}

	@Override
	public void str2(String group, String key, String value, int expire) {
		cache1.str(group, key, value, expire);
		cache2.str(group, key, value, expire);
	}

	@Override
	public void str2(String group, String key, String value) {
		cache1.str(group, key, value);
		cache2.str(group, key, value);
	}

	@Override
	public double incr(String group, String key, double delta, int scale) {
		return cache1.incr(group, key, delta, scale);
	}

	@Override
	public double incr2(String group, String key, double delta, int scale) {
		double d = cache2.incr(group, key, delta, scale);
		cache1.str(group, key, String.valueOf(d));
		return d;
	}

	@Override
	public void num(String group, String key, double num) {
		this.str(group, key, String.valueOf(num));
	}

	@Override
	public void num(String group, String key, int num) {
		this.str(group, key, String.valueOf(num));
	}

	@Override
	public long ttl(String group, String key) {
		return cache2.ttl(group, key);
	}

	@Override
	public void expire(String group, String key, int expire) {
		// TODO 暂不支持
		cache1.expire(group, key, expire);
		cache2.expire(group, key, expire);
	}

	@Override
	public boolean setnx(String group, String key, String value, int expire) {
		// TODO 暂不支持
		cache1.setnx(group, key, value, expire);
		return cache2.setnx(group, key, value, expire);
	}
}
