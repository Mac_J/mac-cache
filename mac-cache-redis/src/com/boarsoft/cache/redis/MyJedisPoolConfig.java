package com.boarsoft.cache.redis;

import java.io.Serializable;

import redis.clients.jedis.JedisPoolConfig;

public class MyJedisPoolConfig implements Serializable {
	private static final long serialVersionUID = -3527524587733749756L;

	protected int maxTotal = JedisPoolConfig.DEFAULT_MAX_TOTAL;

	protected int maxIdle = JedisPoolConfig.DEFAULT_MAX_IDLE;

	protected int minIdle = JedisPoolConfig.DEFAULT_MIN_IDLE;
	
	protected long testInterval = 100L;
	
	protected int testTimes = 3;

	public int getMaxTotal() {
		return maxTotal;
	}

	public void setMaxTotal(int maxTotal) {
		this.maxTotal = maxTotal;
	}

	public int getMaxIdle() {
		return maxIdle;
	}

	public void setMaxIdle(int maxIdle) {
		this.maxIdle = maxIdle;
	}

	public int getMinIdle() {
		return minIdle;
	}

	public void setMinIdle(int minIdle) {
		this.minIdle = minIdle;
	}

	public long getTestInterval() {
		return testInterval;
	}

	public void setTestInterval(long testInterval) {
		this.testInterval = testInterval;
	}

	public int getTestTimes() {
		return testTimes;
	}

	public void setTestTimes(int testTimes) {
		this.testTimes = testTimes;
	}
}