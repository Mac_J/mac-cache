package com.boarsoft.cache.redis;

import java.io.Closeable;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.boarsoft.cache.CacheClient;
import com.boarsoft.cache.config.CacheCatalog;
import com.boarsoft.cache.config.CacheDb;
import com.boarsoft.cache.config.CacheMapping;
import com.boarsoft.cache.config.CacheNode;
import com.boarsoft.cache.config.CacheShard;
import com.boarsoft.cache.monitor.CacheWarmer;

/**
 * 一个SimpleRedisClientImpl只连一个Redis多实例集群<br>
 * 根据 group 与 db 的映射关系，决定某个group在哪个实例上<br>
 * TODO 暂不支持分片的增删（ReHash）
 * 
 * @author Mac_J
 *
 */
public class SimpleRedisClientImpl implements CacheClient {
	private static Logger log = LoggerFactory.getLogger(SimpleRedisClientImpl.class);

	/** group 与 db 之间的映射关系 */
	protected CacheMapping mapping;
	/** 由Spring注入，k = dbName */
	protected Map<String, CacheDb> dbMap;
	/** 注入来自远程缓存监控的目录服务 */
	@Autowired
	protected CacheCatalog catalog;
	/** */
	protected boolean required = false;
	/** */
	protected Set<CacheWarmer> warmers;

	@PostConstruct
	public void init() {
		try {
			for (Entry<String, CacheDb> en : dbMap.entrySet()) {
				String dn = en.getKey();
				CacheDb db = en.getValue();
				if (db == null) {
					throw new IllegalStateException(String.format(//
							"CacheDb %s can not be null", dn));
				}
				db.setCode(dn);
				log.info("Init my jedis pool {}", dn);
				// 作为cacheClient拿不到shards是不应该初始化的
				if (db.getShards() == null) {
					List<CacheShard> lt = catalog.getShards(dn);
					if (lt == null || lt.isEmpty()) {
						log.error("Can not get shards info of {} from remote", dn);
						continue;
					}
					db.setShards(lt);
					db.init();
				}
			}
			log.info("Init {} pools successfully", dbMap.size());
		} catch (Exception e) {
			if (required) {
				throw e;
			}
			log.warn("Ignored exception on init", e);
		}
	}

	@Override
	public Object switch2(String dbCode, int shardIndex, CacheNode n1) {
		CacheDb db = dbMap.get(dbCode);
		if (db == null) {
			log.info("Ignore switch2 cache db {} since the code not found", dbCode);
			return false;
		}
		// 检查分片信息是否匹配（原则上，同一db下的不同shard的master应该不同
		List<CacheShard> shards = db.getShards();
		if (shards.size() > shardIndex) {
			CacheShard cs = shards.get(shardIndex);
			cs.switch2(n1); // 本地切换
			return true;
		}
		// 如果分片信息对不上，则全部重新加载一次（这种情况出现的机率应该较低，留待观察）
		log.warn("Reload cache shards of db {} from remote catalog", dbCode);
		List<CacheShard> lt = catalog.getShards(dbCode);
		if (lt == null || lt.isEmpty()) {
			log.error("Can not get shards info of {} from remote", dbCode);
			return false;
		}
		db.setShards(lt);
		db.init();
		return true;
	}

	@Override
	public boolean warmUp(String dbCode, int shardIndex) {
		if (warmers == null || warmers.isEmpty()) {
			log.warn("Can not warm up master cache node of shard {}/{} because".concat(//
					" warmer map of client {} is emtpy"), dbCode, shardIndex, this);
			return true;
		}
		Map<String, String> gm = mapping.getGroupMap();
		for (Entry<String, String> en : gm.entrySet()) {
			String group = en.getKey();
			for (CacheWarmer cw : warmers) {
				if (cw == null) {
					continue;
				}
				try {
					cw.warmUp(dbCode, shardIndex, group);
				} catch (Exception e) {
					log.error("Error on cache warmer {} load group {} to {}/{}", //
							cw, group, dbCode, shardIndex, e);
				}
			}
		}
		return true;
	}

	@Override
	public Object getDbMap() {
		return dbMap;
	}
	
	@Override
	public Map<String, String> getGroupMap() {
		return mapping.getGroupMap();
	}

	@Override
	public void closeAll() {
		for (Entry<String, CacheDb> en : dbMap.entrySet()) {
			this.close(en.getKey());
		}
	}

	@Override
	public void close(String dbName) {
		log.warn("Close simple jedis pool {}", dbName);
		this.close(dbMap.get(dbName));
	}

	@Override
	public void close(Closeable obj) {
		if (obj == null) {
			return;
		}
		try {
			obj.close();
		} catch (Exception e) {
			log.error("Error on close jedis pool", e);
		}
	}

	@Override
	public CacheDb getDbByGroup(String group) {
		String db = mapping.getDbByGroup(group);
		return dbMap.get(db);
	}

	@Override
	public CacheDb getDbByName(String dbName) {
		return dbMap.get(dbName);
	}

	@Override
	public Map<String, Set<String>> getDbMaping() {
		return mapping.getDbMap();
	}

	public void setDbMap(Map<String, CacheDb> dbMap) {
		this.dbMap = dbMap;
	}

	public CacheMapping getMapping() {
		return mapping;
	}

	public void setMapping(CacheMapping mapping) {
		this.mapping = mapping;
	}

	public CacheCatalog getCatalog() {
		return catalog;
	}

	public void setCatalog(CacheCatalog catalog) {
		this.catalog = catalog;
	}

	public boolean isRequired() {
		return required;
	}

	public void setRequired(boolean required) {
		this.required = required;
	}

	public Set<CacheWarmer> getWarmers() {
		return warmers;
	}

	public void setWarmers(Set<CacheWarmer> warmers) {
		this.warmers = warmers;
	}
}
