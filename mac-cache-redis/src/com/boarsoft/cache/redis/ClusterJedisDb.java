package com.boarsoft.cache.redis;

import java.io.IOException;
import java.io.Serializable;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.boarsoft.cache.config.CacheDb;
import com.boarsoft.cache.config.CacheNode;
import com.boarsoft.cache.config.CacheShard;

import redis.clients.jedis.HostAndPort;
import redis.clients.jedis.JedisCluster;

/**
 * JedisCluster + <br>
 * 如果配置了shards表示希望由平台来管理每个分片（主从）<br>
 * 反之表示不管理或用单独的CacheDB来管理<br>
 * 当shards为空时，CacheMonitor不会对其进行健康检查
 * 
 * @author Mac_J
 *
 */
public class ClusterJedisDb implements CacheDb, Serializable {
	private static final long serialVersionUID = -2516973236217154112L;
	private static final Logger log = LoggerFactory.getLogger(ClusterJedisDb.class);

	/** */
	protected String code;
	/** */
	protected List<CacheShard> shards;
	
	/** */
	protected transient JedisCluster jedisCluster;

	@Override
	public void init() {
		// 可以配置shards来让平台管理分片，否则需要自行管理或创建额外的CacheDb来实现
		if (shards.isEmpty()) {
			return;
		}
		// 初始化所有分片的连接（与分片中主节点的连接）
		log.info("Init {} shards for ClusterJedisDb {}", shards.size(), code);
		Set<HostAndPort> set = new HashSet<>();
		for (int i = 0; i < shards.size(); i++) {
			CacheShard cs = shards.get(i);
			// cs.init(code, i);
			CacheNode n = cs.getMaster();
			set.add(new HostAndPort(n.getHost(), n.getPort()));
		}
		jedisCluster = new JedisCluster(set);
		for (int i = 0; i < shards.size(); i++) {
			CacheShard cs = shards.get(i);
			// cs.init(code, i);
			ClusterJedisNode n = (ClusterJedisNode) cs.getMaster();
			n.setJedisCluster(jedisCluster);
		}
	}

	@Override
	public Object getResource(String key) {
		return jedisCluster;
	}

	@Override
	public Object getResource(String key, boolean readOnly) {
		return jedisCluster;
	}

	@Override
	public Map<String, Object> getResourceMap() {
//		Map<String, JedisPool> nm = jedisCluster.getClusterNodes();
		Map<String, Object> rm = new HashMap<String, Object>();
//		for (String k : nm.keySet()) {
//			rm.put(k, nm.get(k).getResource());
//		}
		rm.put("all", jedisCluster);
		return rm;
	}

	@Override
	public void close() throws IOException {
		jedisCluster.close();
	}

	@Override
	public CacheShard getShard(int index) {
		if (index < shards.size()) {
			return shards.get(index);
		}
		log.error("Cache shard index {} for cache db {} is out of range", index, this);
		return null;
	}

	@Override
	public List<CacheShard> getShards() {
		return shards;
	}

	public void setShards(List<CacheShard> shards) {
		this.shards = shards;
	}

	public void setCode(String code) {
		this.code = code;
	}

	@Override
	public String getCode() {
		return code;
	}

}
