package com.boarsoft.cache.redis;

import java.io.Serializable;

import redis.clients.jedis.JedisShardInfo;

public class MyJedisShardInfo extends JedisShardInfo implements Serializable {
	private static final long serialVersionUID = -5436932630693855581L;

	public MyJedisShardInfo(String host, int port) {
		super(host, port);
	}
}
