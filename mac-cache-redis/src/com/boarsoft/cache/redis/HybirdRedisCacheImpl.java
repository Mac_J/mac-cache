package com.boarsoft.cache.redis;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.boarsoft.cache.Cache;
import com.boarsoft.cache.CacheClient;
import com.boarsoft.cache.config.CacheDb;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisCluster;
import redis.clients.jedis.params.SetParams;

/**
 * 支持多模式集群混合部署的缓存（客户端）实现类
 * 
 * @author Mac_J
 *
 */
public class HybirdRedisCacheImpl implements Cache {
	private static final Logger log = LoggerFactory.getLogger(HybirdRedisCacheImpl.class);
	/** */
	protected CacheClient cacheClient;
	/** */
	protected CacheConfig cacheConfig = new CacheConfig();

	@Override
	public boolean exists(String group, String key) throws UnsupportedEncodingException {
		// 根据group获取对应的DB
		CacheDb db = cacheClient.getDbByGroup(group);
		// 生成key，因为一个DB上可能有多个group
		String sk = cacheConfig.getKey(group, key);
		// 是否支持Binary?
		Object ro = db.getResource(sk);
		if (ro instanceof JedisCluster) {
			return ((JedisCluster) ro).exists(sk);
		}
		Jedis jedis = (Jedis) ro;
		try {
			return jedis.exists(sk);
		} finally {
			cacheClient.close(jedis);
		}
	}

	@Override
	public void str(String group, String key, String value) {
		this.str(group, key, value, -1);
	}

	@Override
	public void str(String group, String key, String value, int expire) {
		CacheDb db = cacheClient.getDbByGroup(group);
		String sk = cacheConfig.getKey(group, key);
		Object ro = db.getResource(sk);
		if (ro instanceof JedisCluster) {
			if (expire >= 0) {
				((JedisCluster) ro).setex(sk, expire, value);
			} else {
				((JedisCluster) ro).set(sk, value);
			}
			return;
		}
		Jedis jedis = (Jedis) ro;
		try {
			if (expire >= 0) {
				jedis.setex(sk, expire, value);
			} else {
				jedis.set(sk, value);
			}
		} finally {
			cacheClient.close(jedis);
		}
	}

	@Override
	public boolean setnx(String group, String key, String value, int expire) {
		CacheDb db = cacheClient.getDbByGroup(group);
		String sk = cacheConfig.getKey(group, key);
		Object ro = db.getResource(sk);
		// boolean ok = false;
		if (ro instanceof JedisCluster) {
			if (expire >= 0) {
				SetParams sp = new SetParams();
				sp.nx().ex(expire);
				return "OK".equals(((JedisCluster) ro).set(key, value, sp));
			} else {
				return ((JedisCluster) ro).setnx(sk, value) == 1;
			}
		}
		try (Jedis jedis = (Jedis) ro) {
			if (expire >= 0) {
				SetParams sp = new SetParams();
				sp.nx().ex(expire);
				return "OK".equals(((Jedis) ro).set(sk, value, sp));
			} else {
				return ((Jedis) ro).setnx(sk, value) == 1;
			}
			// Pipeline pl = jedis.pipelined();
			// pl.setnx(sk, value);
			// if (ok && expire >= 0) {
			// pl.expire(sk, expire);
			// }
			// List<Object> lt = pl.syncAndReturnAll();
			// Long r = new Long(1L);
			// for (Object o : lt) {
			// log.debug("Redis setnx pipeline return {}", o);
			// if (!r.equals(o)) {
			// return false;
			// }
			// }
			// return true;
		}
	}

	@Override
	public String str(String group, String key) {
		CacheDb db = cacheClient.getDbByGroup(group);
		String sk = cacheConfig.getKey(group, key);
		Object ro = db.getResource(sk);
		if (ro instanceof JedisCluster) {
			return ((JedisCluster) ro).get(sk);
		}
		try (Jedis jc = (Jedis) ro;) {
			return jc.get(sk);
		}
	}

	@Override
	public Object get(String group, String key) throws Exception {
		String sk = cacheConfig.getKey(group, key);
		// 根据group获取对应的DB
		CacheDb db = cacheClient.getDbByGroup(group);
		Map<byte[], byte[]> bm = this.hgetAll(db, sk);
		if (bm == null || bm.isEmpty()) {
			return null;
		}
		byte[] cv = bm.remove(cacheConfig.getClassKey());
		if (cv == null || cv.length < 1) {
			throw new IllegalStateException("Invalid redis cache format on ".concat(sk));
		}
		String cn = new String(cv, cacheConfig.getCharset());
		Object ov = null;
		if (bm.containsKey(cacheConfig.getValueKey())) {
			byte[] bv = bm.get(cacheConfig.getValueKey());
			ov = CacheConfig.deserialize(bv);
		} else {
			ov = cacheConfig.decodeObj(sk, cn, bm);
		}
		return ov;
	}

	@SuppressWarnings("unchecked")
	@Override
	public void clear() {
		log.warn("Clear all redis cache db");
		// 获取并遍历所有的db
		Map<String, CacheDb> dbMap = (Map<String, CacheDb>) cacheClient.getDbMap();
		for (Entry<String, CacheDb> en : dbMap.entrySet()) {
			String d = en.getKey();
			log.warn("Clear redis cache db {}", d);
			CacheDb db = cacheClient.getDbByName(d);
			// 获取当前db下所有的数据源（分片）
			Map<String, Object> rm = db.getResourceMap();
			for (Entry<String, Object> en2 : rm.entrySet()) {
				String c = en2.getKey();
				Object o = en2.getValue();
				log.warn("Clear redis cache db {} = {}", c, o);
				this.clear(o, "*");
			}
		}
	}

	protected void clear(Object jo, String pattern) {
		if (jo instanceof JedisCluster) {
			Set<String> keys = ((JedisCluster) jo).hkeys(pattern);
			int size = keys.size();
			if (size > 0) {
				log.warn("Gona del {} keys at once", size);
				String[] ka = new String[size];
				keys.toArray(ka);
				((JedisCluster) jo).del(ka);
			}
			return;
		}
		try (Jedis jedis = (Jedis) jo) {
			Set<String> keys = jedis.keys(pattern);
			int size = keys.size();
			if (size > 0) {
				log.warn("Gona del {} keys at once", size);
				String[] ka = new String[size];
				keys.toArray(ka);
				jedis.del(ka);
			}
		}
	}

	protected Map<String, Object> list(Jedis jedis, String group) throws Exception {
		Map<String, Object> rm = new HashMap<String, Object>();
		String pattern = String.format("%s\\.*", group);
		// String pattern = "*";
		// Set<byte[]> bkSet = jedis.keys(serializer.serialize(pattern));
		String charset = cacheConfig.getCharset();
		byte[] classKey = cacheConfig.getClassKey();
		byte[] valueKey = cacheConfig.getValueKey();
		Set<byte[]> bkSet = jedis.keys(pattern.getBytes(charset));
		for (byte[] bk : bkSet) {
			String sk = new String(bk, charset);
			String kt = jedis.type(sk);
			log.debug("Get redis key {}/{} from {}", kt, sk, jedis);
			if ("hash".equals(kt)) {
				Map<byte[], byte[]> bm = jedis.hgetAll(bk);
				String cn = new String(bm.remove(classKey), charset);
				Object ov = null;
				if (bm.containsKey(valueKey)) {
					byte[] bv = bm.get(valueKey);
					ov = CacheConfig.deserialize(bv);
				} else {
					ov = cacheConfig.decodeObj(sk, cn, bm);
				}
				rm.put(sk, ov);
			} else if ("string".equals(kt)) {
				rm.put(sk, jedis.get(sk));
			} else {
				log.warn("Ignore {}/{} from jedis", kt, sk, jedis);
			}
		}
		return rm;
	}

	@Override
	public long incr(String group, String key, long delta) {
		CacheDb db = cacheClient.getDbByGroup(group);
		String sk = cacheConfig.getKey(group, key);
		Object ro = db.getResource(sk);
		if (ro instanceof JedisCluster) {
			return ((JedisCluster) ro).incrBy(sk, delta);
		}
		Jedis jedis = (Jedis) ro;
		try {
			return jedis.incrBy(sk, delta);
		} finally {
			cacheClient.close(jedis);
		}
	}

	@Override
	public double incr(String group, String key, double delta, int scale) {
		CacheDb db = cacheClient.getDbByGroup(group);
		String sk = cacheConfig.getKey(group, key);
		Object ro = db.getResource(sk);
		if (ro instanceof JedisCluster) {
			return ((JedisCluster) ro).incrByFloat(sk, delta);
		}
		Jedis jedis = (Jedis) ro;
		try {
			return jedis.incrByFloat(sk, delta);
		} finally {
			cacheClient.close(jedis);
		}
	}

	@Override
	public void set(String group, String key, Map<String, Object> om) throws Exception {
		this.set(group, key, om, -1);
	}

	protected Map<byte[], byte[]> hgetAll(CacheDb db, String sk) throws UnsupportedEncodingException {
		byte[] bk = cacheConfig.getBytes(sk);
		Object ro = db.getResource(sk);
		if (ro instanceof JedisCluster) {
			return ((JedisCluster) ro).hgetAll(bk);
		}
		Jedis jedis = (Jedis) ro;
		try {
			return jedis.hgetAll(bk);
		} finally {
			cacheClient.close(jedis);
		}
	}

	@Override
	public void get(String group, String key, Map<String, Object> om) throws Exception {
		CacheDb db = cacheClient.getDbByGroup(group);
		String sk = cacheConfig.getKey(group, key);
		Map<byte[], byte[]> bm = this.hgetAll(db, sk);
		for (Entry<String, Object> en : om.entrySet()) {
			String sf = en.getKey();
			byte[] bf = sf.getBytes(cacheConfig.getCharset());
			if (bm.containsKey(bf)) {
				byte[] bv = bm.get(bf);
				Object ov = CacheConfig.deserialize(bv);
				if (ov != null || om.get(sf) == null) {
					om.put(sf, ov);
				}
			}
		}
	}

	@Override
	public void put(String group, String key, Object value) throws Exception {
		this.put(group, key, value, -1);
	}

	@Override
	public void put(String group, String key, Object ov, int expire) throws Exception {
		CacheDb db = cacheClient.getDbByGroup(group);
		String sk = cacheConfig.getKey(group, key);
		byte[] bk = cacheConfig.getBytes(sk);
		Map<byte[], byte[]> bm = null;
		if (ov != null) {
			String cn = ov.getClass().getName();
			// 如果此类名在types中存在，表示该类需要以HMAP形式存储
			// if (ov instanceof String) {
			// this.put(db, sk, (String) ov, expire);
			// return;
			// }
			if (cacheConfig.getTypes().contains(cn)) {
				log.debug("Type {} need encode", cn);
				bm = cacheConfig.encodeObj(ov);
			} else {
				byte[] bv = CacheConfig.serialize(ov);
				bm = new HashMap<byte[], byte[]>();
				bm.put(cacheConfig.getClassKey(), cn.getBytes(cacheConfig.getCharset()));
				bm.put(cacheConfig.getValueKey(), bv);
			}
		}
		this.put(db, sk, bk, bm, expire);
	}

	protected void put(CacheDb db, String sk, String ov, int expire) {
		Object ro = db.getResource(sk);
		if (ro instanceof JedisCluster) {
			if (expire >= 0) {
				((JedisCluster) ro).setex(sk, expire, ov);
			} else {
				((JedisCluster) ro).set(sk, ov);
			}
			return;
		}
		try (Jedis jedis = (Jedis) ro) {
			if (expire >= 0) {
				jedis.setex(sk, expire, ov);
			} else {
				jedis.set(sk, ov);
			}
		}
	}

	protected void put(CacheDb db, String sk, byte[] bk, Map<byte[], byte[]> bm, int expire) {
		Object ro = db.getResource(sk);
		if (ro instanceof JedisCluster) {
			if (bm == null) {
				((JedisCluster) ro).del(bk);
				return;
			}
			((JedisCluster) ro).hmset(bk, bm);
			if (expire >= 0) {
				((JedisCluster) ro).expire(bk, expire);
			}
			return;
		}
		try (Jedis jedis = (Jedis) ro) {
			if (bm == null) {
				jedis.del(bk);
				return;
			}
			jedis.hmset(bk, bm);
			if (expire >= 0) {
				jedis.expire(bk, expire);
			}
		}
	}

	@Override
	public void set(String group, String key, Map<String, Object> om, int expire) throws Exception {
		CacheDb db = cacheClient.getDbByGroup(group);
		String sk = cacheConfig.getKey(group, key);
		byte[] bk = cacheConfig.getBytes(sk);
		Map<byte[], byte[]> bm = new HashMap<byte[], byte[]>();
		for (Entry<String, Object> en : om.entrySet()) {
			String k = en.getKey();
			Object f = om.get(k);
			bm.put(cacheConfig.getBytes(k), CacheConfig.serialize(f));
		}
		Object ro = db.getResource(sk);
		if (ro instanceof JedisCluster) {
			((JedisCluster) ro).hmset(bk, bm);
			if (expire >= 0) {
				((JedisCluster) ro).expire(bk, expire);
			}
			return;
		}
		try (Jedis jedis = (Jedis) ro;) {
			jedis.hmset(bk, bm);
			if (expire >= 0) {
				jedis.expire(bk, expire);
			}
		}
	}

	@Override
	public void remove(String group) {
		CacheDb db = cacheClient.getDbByGroup(group);
		String sk = new StringBuilder(group).append("\\.*").toString();
		Map<String, Object> rm = db.getResourceMap();
		for (Object o : rm.values()) {
			this.clear(o, sk);
		}
	}

	@Override
	public void remove(String group, String key) throws UnsupportedEncodingException {
		CacheDb db = cacheClient.getDbByGroup(group);
		String sk = cacheConfig.getKey(group, key);
		// byte[] bk = cacheConfig.getBytes(sk);
		Object ro = db.getResource(sk, false);
		if (ro instanceof JedisCluster) {
			((JedisCluster) ro).del(sk); // 用sk与bk都是一样的
			return;
		}
		Jedis jedis = (Jedis) ro;
		try {
			jedis.del(sk);
		} finally {
			cacheClient.close(jedis);
		}
	}

	@Override
	public Map<String, Object> list(String dbName, String group) throws Exception {
		CacheDb db = cacheClient.getDbByName(dbName);
		Map<String, Object> rm = db.getResourceMap();
		for (Object o : rm.values()) {
			Jedis j = (Jedis) o;
			Map<String, Object> tm = null;
			try {
				tm = this.list(j, group);
			} finally {
				cacheClient.close(j);
			}
			if (tm == null || tm.isEmpty()) {
				continue;
			}
			rm.putAll(tm);
		}
		return rm;
	}

	@Override
	public Map<String, Object> list(String group) throws Exception {
		CacheDb db = cacheClient.getDbByGroup(group);
		Map<String, Object> rm = db.getResourceMap();
		for (Object o : rm.values()) {
			Jedis j = (Jedis) o;
			Map<String, Object> tm = null;
			try {
				tm = this.list(j, group);
			} finally {
				cacheClient.close(j);
			}
			if (tm == null || tm.isEmpty()) {
				continue;
			}
			rm.putAll(tm);
		}
		return rm;
	}

	@Override
	public Map<String, Set<String>> getDbMaping() {
		return cacheClient.getDbMaping();
	}

	public CacheClient getCacheClient() {
		return cacheClient;
	}

	public void setCacheClient(CacheClient cacheClient) {
		this.cacheClient = cacheClient;
	}

	public CacheConfig getCacheConfig() {
		return cacheConfig;
	}

	public void setCacheConfig(CacheConfig cacheConfig) {
		this.cacheConfig = cacheConfig;
	}

	@Override
	public void num(String group, String key, double num) {
		this.str(group, key, String.valueOf(num));
	}

	@Override
	public void num(String group, String key, int num) {
		this.str(group, key, String.valueOf(num));
	}

	@Override
	public long ttl(String group, String key) {
		CacheDb db = cacheClient.getDbByGroup(group);
		String sk = cacheConfig.getKey(group, key);
		Object ro = db.getResource(sk);
		if (ro instanceof JedisCluster) {
			return ((JedisCluster) ro).ttl(sk);
		}
		Jedis jedis = (Jedis) ro;
		try {
			return jedis.ttl(sk);
		} finally {
			cacheClient.close(jedis);
		}
	}

	@Override
	public void expire(String group, String key, int expire) {
		CacheDb db = cacheClient.getDbByGroup(group);
		String sk = cacheConfig.getKey(group, key);
		Object ro = db.getResource(sk);
		if (ro instanceof JedisCluster) {
			((JedisCluster) ro).expire(sk, expire);
			return;
		}
		try (Jedis jedis = (Jedis) ro;) {
			jedis.expire(sk, expire);
		}
	}
}
