package com.boarsoft.cache.redis;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;

import com.boarsoft.cache.CacheClient;
import com.boarsoft.cache.config.CacheNode;
import com.boarsoft.cache.config.CacheShard;
import com.boarsoft.cache.event.CacheEvent;
import com.boarsoft.cache.monitor.CacheMonitor;
import com.boarsoft.common.util.RandomUtil;

/**
 * 一个shard有多个node，其中只有一个主节点，N个逻辑上的备用节点
 * 
 * @author Mac_J
 *
 */
public class MyJedisShard implements CacheShard {
	private static final long serialVersionUID = 8371468921805735529L;
	private static final Logger log = LoggerFactory.getLogger(MyJedisShard.class);

	public static final String WRONG_STATUS = "Cache node %s is %d, but %d is required";

	protected int type;
	/** 主节点信息，作为客户端和监控节点时都要用到 */
	protected CacheNode master;
	/** 备用节点信息，仅当作为 监控节点时用到 */
	protected Map<String, CacheNode> backups = new ConcurrentHashMap<String, CacheNode>();
	/** 备用节点的后备形式 */
	protected short standby = CacheShard.STANDBY_WARM;
	/** 分片的索引编号（hash） */
	protected int index;
	/** */
	protected String dbCode;

	/** 远程接口，用于广播通知所有客户端主备切换 */
	@Lazy
	@Autowired
	protected transient CacheMonitor cacheMonitor;

	@Override
	public void init(String dbCode, int index) {
		this.dbCode = dbCode;
		this.index = index;
//		if (cacheMonitor == null) {
//			master.open();
//		}
	}

	@Override
	public void close() {
		master.close();
		for (CacheNode bn : backups.values()) {
			bn.close();
		}
	}

	@Override
	public Object getResource() {
		return master.getResource();
	}

	@Override
	public Object getResource(boolean readOnly) {
		if (!readOnly || backups.isEmpty()) {
			return master.getResource();
		}
		int i = RandomUtil.random(0, backups.size());
		return backups.get(String.valueOf(i));
	}

	/**
	 * 执行此方法之前，n1已经过了测试
	 */
	@Override
	public boolean switch2(CacheNode n1) {
		if (master.getCode().equals(n1.getCode())) {
			log.warn("Currrent master cache node already be {}", n1);
			return true;
		}
		log.info("Switch master cache node from {} to {}", master, n1);
		// 执行此方法时，n1应为可写状态
		switch (n1.getStatus()) {
		case CacheNode.STATUS_DOWN:
			if (n1.startUp()) {
				break; // 尝试启动
			}
			return false;
		case CacheNode.STATUS_READ:
			if (n1.toMaster()) {
				break; // 强制将其转为master
			}
			return false;
		}
		// 本地主备切换
		CacheNode n0 = master;
		master.close();
		master = n1; // 指向并连接新的主节点
		master.test();
		// 更新备用节点列表，将原来的主节点放入备用节点列表
		backups.remove(n1.getCode());
		backups.put(n0.getCode(), n0);
		// 如果是主从模式，把原来的主机n0，转成备机
		if (standby == CacheShard.STANDBY_SLAVE) {
			String mh = String.format("%s:%d", n1.getHost(), n1.getPort());
			if (!n0.toSlave(mh)) {
				return false; // 如果切换不成功，则返回false
			}
		}
		log.warn("Switch master cache node from {} to {} successfully", n0, n1);
		return master.isWriteable();
	}

	/**
	 * 执行此方法之前，n1已经过了测试
	 */
	@Override
	public boolean switch2(CacheNode n1, CacheClient cc) {
		// 先执行本地切换
		if (this.switch2(n1)) {
			// 此方法是广播方法，仅让客户端切换（不应在此方法中执行warmUp）
			cc.switch2(dbCode, index, n1);
			return true;
		}
		return false;
	}

	@Override
	public boolean test() {
		CacheNode mn = this.getMaster();
		boolean result = mn.test() == CacheNode.STATUS_WRITE;
		// 当未配置备用节点时，主节点宕机就算宕机
		if (backups == null || backups.isEmpty()) {
			return result;
		}
		// 当有配置备用节点时，依次检查备机，当主节点失效时，遇到合适的备机就进行主备切换
		for (CacheNode bn : backups.values()) {
			short s = CacheNode.STATUS_DOWN;
			try {
				s = bn.test();// 测试当前节点
				switch (standby) {
				case CacheShard.STANDBY_COLD:
					if (s != CacheNode.STATUS_DOWN) {
						String msg = String.format(WRONG_STATUS,//
								bn.toString(), s, CacheNode.STATUS_DOWN);
						log.error(msg);
						cacheMonitor.onEvent(new CacheEvent(//
								CacheEvent.NODE_STATUS, dbCode, bn, msg));
						continue;
					}
					break;
				case CacheShard.STANDBY_SLAVE:
					if (s != CacheNode.STATUS_READ) {
						String msg = String.format(WRONG_STATUS,//
								bn.toString(), s, CacheNode.STATUS_READ);
						log.error(msg);
						cacheMonitor.onEvent(new CacheEvent(//
								CacheEvent.NODE_STATUS, dbCode, bn, msg));
						continue;
					}
					break;
				case CacheShard.STANDBY_WARM:
					if (s != CacheNode.STATUS_WRITE) {
						String msg = String.format(WRONG_STATUS,//
								bn.toString(), s, CacheNode.STATUS_WRITE);
						log.error(msg);
						cacheMonitor.onEvent(new CacheEvent(//
								CacheEvent.NODE_STATUS, dbCode, bn, msg));
						continue;
					}
					break;
				}
			} catch (Exception e) {
				log.error("Error on test cache node {}", bn, e);
				continue;
			}
			// 不管备用节点测试是否成功，如果前面已经确立了新的主节点（主节点必须可写），直接跳过
			if (result) {
				continue;
			}
			switch (standby) {
			case CacheShard.STANDBY_COLD:
				if (!bn.startUp()) {
					continue;// 没启动成功则尝试下一个节点
				}
				break;
			case CacheShard.STANDBY_SLAVE:
				if (!bn.toMaster()) {
					continue; // 如果转主节点不成功，就转尝试下一个节点
				}
				break;
			case CacheShard.STANDBY_WARM:
				// switch2时总是调用cacheClient的warmUp，故这里不用调
				break;
			}
			// 先执行本地主备切换，再通知的所有客户端执行主备切换
			if (this.switch2(bn)) {
				// 通知所有客户端组，通知其下所有客户端切换并装载数据
				cacheMonitor.switch2(dbCode, index, bn);
				return true;
			}
			return false;
		}
		return result;
	}

	@Override
	public short getStatus() {
		return master == null ? CacheNode.STATUS_DOWN : master.getStatus();
	}

	@Override
	public String getDbCode() {
		return dbCode;
	}

	public void setDbCode(String dbCode) {
		this.dbCode = dbCode;
	}

	@Override
	public Map<String, CacheNode> getBackups() {
		return backups;
	}

	public void setBackups(List<CacheNode> backupLt) {
		this.backups.clear();
		for (CacheNode n : backupLt) {
			this.backups.put(n.getCode(), n);
		}
	}

	public CacheNode getMaster() {
		return master;
	}

	public void setMaster(CacheNode master) {
		this.master = master;
	}

	public void setStandby(short standby) {
		this.standby = standby;
	}

	public short getStandby() {
		return standby;
	}

	public int getIndex() {
		return index;
	}

	public void setIndex(int index) {
		this.index = index;
	}

	public CacheMonitor getCacheMonitor() {
		return cacheMonitor;
	}

	public void setCacheMonitor(CacheMonitor cacheMonitor) {
		this.cacheMonitor = cacheMonitor;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}
}
