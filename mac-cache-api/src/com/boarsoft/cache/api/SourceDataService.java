package com.boarsoft.cache.api;

import java.util.Map;

public interface SourceDataService {

	/**
	 * 
	 * @param group
	 * @param ver
	 * @return
	 */
	Map<String, Object> query(String group, String ver);

}