package com.boarsoft.cache.spring;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.FactoryBean;
import org.springframework.data.redis.cache.RedisCacheConfiguration;
import org.springframework.data.redis.cache.RedisCacheManager;
import org.springframework.data.redis.cache.RedisCacheWriter;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.util.Assert;

/**
 * 
 */
public class RedisCacheManagerFactory implements FactoryBean<RedisCacheManager> {
	protected RedisConnectionFactory connectionFactory;
	protected Map<String, RedisCacheConfiguration> initialCacheConfigurations = new HashMap<>();
	protected boolean allowInFlightCacheCreation;
	protected RedisCacheManager redisCacheManager;
	protected RedisCacheWriter redisCacheWriter;

	@PostConstruct
	public void init() {
		if (redisCacheWriter == null) {
			Assert.notNull(connectionFactory, "connectionFactory and redisCacheWriter can not be both null");
			redisCacheWriter = RedisCacheWriter.nonLockingRedisCacheWriter(connectionFactory);
		}
		redisCacheManager = new RedisCacheManager(redisCacheWriter, //
				RedisCacheConfiguration.defaultCacheConfig(), //
				initialCacheConfigurations, allowInFlightCacheCreation);
		redisCacheManager.initializeCaches();
	}

	@Override
	public RedisCacheManager getObject() throws Exception {
		return redisCacheManager;
	}

	@Override
	public Class<?> getObjectType() {
		return RedisCacheManager.class;
	}

	public RedisConnectionFactory getConnectionFactory() {
		return connectionFactory;
	}

	public void setConnectionFactory(RedisConnectionFactory connectionFactory) {
		this.connectionFactory = connectionFactory;
	}

	public Map<String, RedisCacheConfiguration> getInitialCacheConfigurations() {
		return initialCacheConfigurations;
	}

	public void setInitialCacheConfigurations(Map<String, RedisCacheConfiguration> initialCacheConfigurations) {
		this.initialCacheConfigurations = initialCacheConfigurations;
	}

	public boolean isAllowInFlightCacheCreation() {
		return allowInFlightCacheCreation;
	}

	public void setAllowInFlightCacheCreation(boolean allowInFlightCacheCreation) {
		this.allowInFlightCacheCreation = allowInFlightCacheCreation;
	}

	public RedisCacheManager getRedisCacheManager() {
		return redisCacheManager;
	}

	public void setRedisCacheManager(RedisCacheManager redisCacheManager) {
		this.redisCacheManager = redisCacheManager;
	}

	public RedisCacheWriter getRedisCacheWriter() {
		return redisCacheWriter;
	}

	public void setRedisCacheWriter(RedisCacheWriter redisCacheWriter) {
		this.redisCacheWriter = redisCacheWriter;
	}

}
