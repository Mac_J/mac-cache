package com.boarsoft.cache.spring.service.impl;

import java.util.Collections;
import java.util.concurrent.TimeUnit;

import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.script.DefaultRedisScript;

import com.boarsoft.cache.spring.api.LockService;

/**
 * @Description:
 * @Author: 刘超
 * @Date: 2023-07-18 20:33
 */
public class LockServiceImpl implements LockService {

	/**
	 * 脚本执行结果-成功
	 */
	private static final Long FLAG_SUCCESS = 1L;

	/**
	 * 脚本执行结果-失败
	 */
	// private static final Long FLAG_FAIL = 0L;

	/**
	 * 释放锁lua脚本
	 */
	private static final String LUA_DEL_SCRIPT = "if redis.call('get', KEYS[1]) == ARGV[1] then return redis.call('del', KEYS[1]) else return 0 end";

	/**
	 * 默认超时时间
	 */
	private Long timeout = 10L;

	/**
	 * 默认超时时间单位
	 */
	private TimeUnit unit = TimeUnit.SECONDS;

	/**
	 * redis操作公共接口
	 */
	RedisTemplate<String, Object> redisTemplate;

	public RedisTemplate<String, Object> getRedisTemplate() {
		return redisTemplate;
	}

	public void setRedisTemplate(RedisTemplate<String, Object> redisTemplate) {
		this.redisTemplate = redisTemplate;
	}

	public Long getTimeout() {
		return timeout;
	}

	public void setTimeout(Long timeout) {
		this.timeout = timeout;
	}

	public TimeUnit getUnit() {
		return unit;
	}

	public void setUnit(TimeUnit unit) {
		this.unit = unit;
	}

	/**
	 * 使用默认超时时间获取锁
	 *
	 * @param lockKey
	 * @param bizId
	 * @return
	 */
	@Override
	public Boolean lock(String lockKey, String bizId) {
		return lock(lockKey, bizId, timeout, unit);
	}

	/**
	 * 获取锁，自定义失效时间
	 *
	 * @param lockKey
	 * @param bizId
	 * @param timeout
	 * @param unit
	 * @return
	 */
	@Override
	public Boolean lock(String lockKey, String bizId, Long timeout, TimeUnit unit) {
		Boolean bLock = redisTemplate.opsForValue().setIfAbsent(lockKey, bizId, timeout, unit);
		return bLock;
	}

	/**
	 * 使用lua脚本释放锁，保证原子性
	 *
	 * @param lockKey
	 * @param bizId
	 * @return
	 */
	@Override
	public Boolean unlock(String lockKey, String bizId) {
		// 使用redis执行lua执行
		DefaultRedisScript<Long> redisScript = new DefaultRedisScript<>();
		redisScript.setScriptText(LUA_DEL_SCRIPT);
		// 设置一下返回值类型 为Long
		// 因为删除判断的时候，返回的0,给其封装为数据类型。如果不封装那么默认返回String 类型，
		// 那么返回字符串与0 会有发生错误。
		redisScript.setResultType(Long.class);
		// 第一个要是script 脚本 ，第二个需要判断的key，第三个就是key所对应的值。
		Long result = (Long) redisTemplate.execute(redisScript, Collections.singletonList(lockKey), bizId);
		return FLAG_SUCCESS.compareTo(result) == 0;
	}

}
