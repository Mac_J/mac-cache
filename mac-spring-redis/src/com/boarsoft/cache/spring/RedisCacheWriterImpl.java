package com.boarsoft.cache.spring;

import java.io.UnsupportedEncodingException;
import java.time.Duration;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.redis.cache.RedisCacheWriter;

import com.boarsoft.cache.CacheClient;
import com.boarsoft.cache.config.CacheDb;
import com.boarsoft.cache.redis.CacheConfig;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisCluster;
import redis.clients.jedis.params.SetParams;

public class RedisCacheWriterImpl implements RedisCacheWriter {
	private static final Logger log = LoggerFactory.getLogger(RedisCacheWriterImpl.class);
	/** */
	protected CacheClient cacheClient;
	/** */
	protected CacheConfig cacheConfig = new CacheConfig();

	protected static byte[] NX_BYTES = "NX".getBytes();
	protected static byte[] EX_BYTES = "EX".getBytes();

	public RedisCacheWriterImpl() throws UnsupportedEncodingException {
		NX_BYTES = "NX".getBytes("UTF-8");
		EX_BYTES = "EX".getBytes("UTF-8");
	}

	@SuppressWarnings("resource")
	@Override
	public void put(String name, byte[] bk, byte[] bv, Duration ttl) {
		int expire = ttl == null ? -1 : new Long(ttl.getSeconds()).intValue();
		CacheDb db = cacheClient.getDbByGroup(name);
		Object ro = db.getResource(name);
		if (ro instanceof JedisCluster) {
			if (expire >= 0) {
				((JedisCluster) ro).setex(bk, expire, bv);
			} else {
				((JedisCluster) ro).set(bk, bv);
			}
			return;
		}
		try (Jedis jedis = (Jedis) ro) {
			if (expire >= 0) {
				jedis.setex(bk, expire, bv);
			} else {
				jedis.set(bk, bv);
			}
		}
	}

	@Override
	public byte[] get(String name, byte[] bk) {
		CacheDb db = cacheClient.getDbByGroup(name);
		Object ro = db.getResource(name);
		if (ro instanceof JedisCluster) {
			return ((JedisCluster) ro).get(bk);
		}
		try (Jedis jedis = (Jedis) ro) {
			return jedis.get(bk);
		}
	}

	@Override
	public byte[] putIfAbsent(String name, byte[] bk, byte[] bv, Duration ttl) {
		int expire = ttl == null ? -1 : new Long(ttl.getSeconds()).intValue();
		CacheDb db = cacheClient.getDbByGroup(name);
		Object ro = db.getResource(name);
		if (ro instanceof JedisCluster) {
			if (expire >= 0) {
				SetParams sp = new SetParams();
				sp.nx().ex(expire);
				if ("OK".equals(((JedisCluster) ro).set(bk, bv, sp))) {
					return bv;
				}
			} else {
				if (((JedisCluster) ro).setnx(bk, bv) == 1) {
					return bv;
				}
			}
		}
		try (Jedis jedis = (Jedis) ro) {
			if (expire >= 0) {
				SetParams sp = new SetParams();
				sp.nx().ex(expire);
				if ("OK".equals(((Jedis) ro).set(bk, bv, sp))) {
					return bv;
				}
			} else {
				if (((Jedis) ro).setnx(bk, bv) == 1) {
					return bv;
				}
			}
		}
		return null;
	}

	@Override
	public void remove(String name, byte[] bk) {
		CacheDb db = cacheClient.getDbByGroup(name);
		Object ro = db.getResource(name);
		if (ro instanceof JedisCluster) {
			((JedisCluster) ro).del(bk); // 用sk与bk都是一样的
			return;
		}
		try (Jedis jedis = (Jedis) ro) {
			jedis.del(bk);
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public void clean(String name, byte[] pattern) {
		log.warn("Clear all redis cache db");
		// 获取并遍历所有的db
		Map<String, CacheDb> dbMap = (Map<String, CacheDb>) cacheClient.getDbMap();
		for (String d : dbMap.keySet()) {
			log.warn("Clear redis cache db {}", d);
			CacheDb db = cacheClient.getDbByName(d);
			// 获取当前db下所有的数据源（分片）
			Map<String, Object> rm = db.getResourceMap();
			for (String c : rm.keySet()) {
				log.warn("Clear redis cache db {} = {}", c, rm.get(c));
				this.clear(rm.get(c), "*");
			}
		}
	}

	protected void clear(Object jo, String pattern) {
		if (jo instanceof JedisCluster) {
			Set<String> keys = ((JedisCluster) jo).hkeys(pattern);
			int size = keys.size();
			if (size > 0) {
				log.warn("Gona del {} keys at once", size);
				String[] ka = new String[size];
				keys.toArray(ka);
				((JedisCluster) jo).del(ka);
			}
			return;
		}
		try (Jedis jedis = (Jedis) jo) {
			Set<String> keys = jedis.keys(pattern);
			int size = keys.size();
			if (size > 0) {
				log.warn("Gona del {} keys at once", size);
				String[] ka = new String[size];
				keys.toArray(ka);
				jedis.del(ka);
			}
		}
	}

	public CacheClient getCacheClient() {
		return cacheClient;
	}

	public void setCacheClient(CacheClient cacheClient) {
		this.cacheClient = cacheClient;
	}

	public CacheConfig getCacheConfig() {
		return cacheConfig;
	}

	public void setCacheConfig(CacheConfig cacheConfig) {
		this.cacheConfig = cacheConfig;
	}

}
