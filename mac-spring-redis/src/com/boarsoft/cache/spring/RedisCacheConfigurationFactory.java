package com.boarsoft.cache.spring;

import java.time.Duration;

import org.springframework.beans.factory.FactoryBean;
import org.springframework.core.convert.ConversionService;
import org.springframework.data.redis.cache.RedisCacheConfiguration;
import org.springframework.data.redis.cache.RedisCacheManager;
import org.springframework.data.redis.serializer.RedisSerializationContext.SerializationPair;
import org.springframework.data.redis.serializer.RedisSerializer;

/**
 * 
 */
public class RedisCacheConfigurationFactory implements FactoryBean<RedisCacheConfiguration> {
	private Integer ttl;
	private boolean cacheNullValues = true;
	private String keyPrefix;
	private boolean usePrefix = true;
	private ConversionService conversionService;
	private RedisSerializer<String> keySerializer;
	private RedisSerializer<?> valueSerializer;

	private RedisCacheConfiguration configuration;

	@Override
	public RedisCacheConfiguration getObject() throws Exception {
		configuration = RedisCacheConfiguration.defaultCacheConfig();
		if (ttl != null) {
			configuration = configuration.entryTtl(Duration.ofSeconds(ttl));
		}
		if (!cacheNullValues) {
			configuration = configuration.disableCachingNullValues();
		}
		if (keyPrefix != null && !"".equals(keyPrefix.trim())) {
			configuration = configuration.prefixKeysWith(keyPrefix);
		}
		if (!usePrefix) {
			configuration = configuration.disableKeyPrefix();
		}
		if (keySerializer != null) {
			configuration = configuration.serializeKeysWith(//
					SerializationPair.fromSerializer(keySerializer));
		}
		if (valueSerializer != null) {
			configuration = configuration.serializeValuesWith(//
					SerializationPair.fromSerializer(valueSerializer));
		}
		if (conversionService != null) {
			configuration = configuration.withConversionService(conversionService);
		}
		return configuration;
	}

	@Override
	public Class<?> getObjectType() {
		return RedisCacheManager.class;
	}

	public Integer getTtl() {
		return ttl;
	}

	public void setTtl(Integer ttl) {
		this.ttl = ttl;
	}

	public boolean isCacheNullValues() {
		return cacheNullValues;
	}

	public void setCacheNullValues(boolean cacheNullValues) {
		this.cacheNullValues = cacheNullValues;
	}

	public String getKeyPrefix() {
		return keyPrefix;
	}

	public void setKeyPrefix(String keyPrefix) {
		this.keyPrefix = keyPrefix;
	}

	public boolean isUsePrefix() {
		return usePrefix;
	}

	public void setUsePrefix(boolean usePrefix) {
		this.usePrefix = usePrefix;
	}

	public ConversionService getConversionService() {
		return conversionService;
	}

	public void setConversionService(ConversionService conversionService) {
		this.conversionService = conversionService;
	}

	public RedisSerializer<String> getKeySerializer() {
		return keySerializer;
	}

	public void setKeySerializer(RedisSerializer<String> keySerializer) {
		this.keySerializer = keySerializer;
	}

	public RedisSerializer<?> getValueSerializer() {
		return valueSerializer;
	}

	public void setValueSerializer(RedisSerializer<?> valueSerializer) {
		this.valueSerializer = valueSerializer;
	}

}
