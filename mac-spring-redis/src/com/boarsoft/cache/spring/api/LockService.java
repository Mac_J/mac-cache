package com.boarsoft.cache.spring.api;

import java.util.concurrent.TimeUnit;

/**
 * @Description: redis分布式锁服务
 * @Author: 刘超
 * @Date: 2023-07-18 19:32
 */
public interface LockService {

    /**
     * 尝试获取锁，默认超时时间
     *
     * @param lockKey
     * @param bizId
     * @return
     */
    Boolean lock(String lockKey, String bizId);

    /**
     * 尝试获取锁并设置尝试时间
     *
     * @param lockKey
     * @param bizId
     * @param timeout
     * @param unit
     * @return
     */
    Boolean lock(String lockKey, String bizId, Long timeout, TimeUnit unit);

    /**
     * 尝试释放锁
     *
     * @param lockKey
     * @return
     */
    Boolean unlock(String lockKey,String bizId);
}
