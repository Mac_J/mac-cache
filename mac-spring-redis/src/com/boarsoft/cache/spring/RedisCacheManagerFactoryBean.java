package com.boarsoft.cache.spring;

import java.util.Map;

import org.springframework.beans.factory.FactoryBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.cache.RedisCacheConfiguration;
import org.springframework.data.redis.cache.RedisCacheWriter;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.serializer.RedisSerializer;
import org.springframework.data.redis.serializer.StringRedisSerializer;

/**
 * @author user
 */
// @Service
public class RedisCacheManagerFactoryBean implements FactoryBean<ComboRedisCacheManager> {
	@Autowired(required = false)
	private RedisConnectionFactory redisConnectionFactory;

	private RedisSerializer<?> serializer = new StringRedisSerializer();

	private Map<String, RedisCacheConfiguration> configurationMap;

	@Override
	public ComboRedisCacheManager getObject() throws Exception {
		// 初始化一个RedisCacheWriter
		RedisCacheWriter cacheWriter = //
				RedisCacheWriter.nonLockingRedisCacheWriter(redisConnectionFactory);
		// 设置CacheManager的值序列化方式为json序列化
		// RedisSerializationContext.SerializationPair<?> pair = //
		// RedisSerializationContext.SerializationPair.fromSerializer(serializer);
		// RedisCacheConfiguration defaultCacheConfig = //
		// RedisCacheConfiguration.defaultCacheConfig()//
		// .serializeValuesWith(pair);
		// 设置默认超过时期是1天
		// defaultCacheConfig.entryTtl(Duration.ofDays(1));
		// 初始化RedisCacheManager
		ComboRedisCacheManager redisCacheManager = new ComboRedisCacheManager(cacheWriter,
				RedisCacheConfiguration.defaultCacheConfig(), configurationMap);
		redisCacheManager.initializeCaches();
		return redisCacheManager;
	}

	@Override
	public Class<?> getObjectType() {
		return ComboRedisCacheManager.class;
	}

	public RedisConnectionFactory getRedisConnectionFactory() {
		return redisConnectionFactory;
	}

	public void setRedisConnectionFactory(RedisConnectionFactory redisConnectionFactory) {
		this.redisConnectionFactory = redisConnectionFactory;
	}

	public RedisSerializer<?> getSerializer() {
		return serializer;
	}

	public void setSerializer(RedisSerializer<?> serializer) {
		this.serializer = serializer;
	}

	public Map<String, RedisCacheConfiguration> getConfigurationMap() {
		return configurationMap;
	}

	public void setConfigurationMap(Map<String, RedisCacheConfiguration> configurationMap) {
		this.configurationMap = configurationMap;
	}

}