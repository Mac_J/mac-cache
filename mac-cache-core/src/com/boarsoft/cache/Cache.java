package com.boarsoft.cache;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.Map;
import java.util.Set;

/**
 * 缓存API
 * 
 * @author Mac_J
 *
 */
public interface Cache {
	/**
	 * 缓存数据value到指定分组
	 * 
	 * @param group
	 * @param key
	 * @param value
	 *            要缓存的数组
	 * @throws Exception
	 */
	void put(String group, String key, Object value) throws Exception;

	/**
	 * 缓存数据到指定分组
	 * 
	 * @param group
	 * @param key
	 * @param value
	 * @param expire
	 * @throws Exception
	 */
	void put(String group, String key, Object value, int expire) throws Exception;

	/**
	 * 从指定分组获取缓存数据
	 * 
	 * @param group
	 * @param key
	 * @return
	 * @throws Exception
	 */
	Object get(String group, String key) throws Exception;

	/**
	 * 清除所有数据（所有分组）
	 */
	void clear();

	/**
	 * 删除某个分组
	 * 
	 * @param group
	 */
	void remove(String group);

	/**
	 * 移除指定分组下的某条数据
	 * 
	 * @param group
	 * @param key
	 * @throws UnsupportedEncodingException
	 */
	void remove(String group, String key) throws UnsupportedEncodingException;

	/**
	 * 返回指定group下所有的数据（键值对）
	 * 
	 * @param group
	 * @return
	 * @throws Exception
	 */
	Map<String, Object> list(String group) throws Exception;

	/**
	 * 加或减指定分组中某个key的（long型）数据
	 * 
	 * @param group
	 * @param key
	 * @param delta
	 * @return
	 */
	long incr(String group, String key, long delta);

	/**
	 * 以map形式（如：redis的hmset）存储一组对象，永不过期
	 * 
	 * @param group
	 * @param key
	 * @param om
	 *            要以map形式存储的所有对象（，这些对象将被序列化成字节数组来存储
	 * @param expire
	 *            超时时间，单位秒
	 * @throws Exception
	 */
	void set(String group, String key, Map<String, Object> om) throws Exception;

	/**
	 * 以map形式（如：redis的hmset）存储一组对象，带过期时间
	 * 
	 * @param group
	 * @param key
	 * @param om
	 *            要以map形式存储的所有对象（，这些对象将被序列化成字节数组来存储
	 * @param expire
	 *            超时时间，单位秒
	 * @throws Exception
	 */
	void set(String group, String key, Map<String, Object> om, int expire) throws Exception;

	/**
	 * 将对map形式存储的对象从缓存中反序列化后还原为Map<String, Object>
	 * 
	 * @param group
	 * @param key
	 * @param om
	 *            用于接收这一组对象的map
	 * @throws Exception
	 */
	void get(String group, String key, Map<String, Object> om) throws Exception;

	/**
	 * 检查某个group组中某个key是否存在
	 * 
	 * @param group
	 * @param key
	 * @return
	 * @throws UnsupportedEncodingException
	 */
	boolean exists(String group, String key) throws UnsupportedEncodingException;

	/**
	 * 查询指定db下指定group的内容
	 * 
	 * @param dbName
	 * @param group
	 * @return
	 * @throws ClassNotFoundException
	 * @throws IOException
	 */
	Map<String, Object> list(String dbName, String group) throws Exception;

	/**
	 * 返回 db -> groups list，一个db下有哪些group
	 * 
	 * @return
	 */
	Map<String, Set<String>> getDbMaping();

	/**
	 * 存入字符串，带过期时间
	 * 
	 * @param group
	 * @param key
	 * @param value
	 * @param expire
	 */
	void str(String group, String key, String value, int expire);

	/**
	 * 存入字符串（存入的字符串可以用str方法、jedis/jedisCluster的get方法获取，但不能用cache.get方法）
	 * 
	 * @param group
	 * @param key
	 * @param value
	 */
	void str(String group, String key, String value);

	/**
	 * 取出字符串
	 * 
	 * @param group
	 * @param key
	 * @return
	 */
	String str(String group, String key);

	/**
	 * 浮点数操作
	 * 
	 * @param group
	 * @param key
	 * @param delta
	 * @param scale
	 * @return
	 */
	double incr(String group, String key, double delta, int scale);

	/**
	 * 设置浮点数
	 * 
	 * @param group
	 * @param key
	 * @param num
	 */
	void num(String group, String key, double num);

	/**
	 * 设置整型
	 * 
	 * @param group
	 * @param key
	 * @param num
	 */
	void num(String group, String key, int num);

	/**
	 * 获取键的失效时间
	 * 
	 * @param group
	 * @param k
	 * @return
	 */
	long ttl(String group, String key);

	/**
	 * 设置键的失效时间
	 * 
	 * @param group
	 * @param key
	 * @param expire
	 * @return
	 */
	void expire(String group, String key, int expire);

	/**
	 * 
	 * @param group
	 * @param key
	 * @param value
	 * @param expire
	 */
	boolean setnx(String group, String key, String value, int expire);
}
