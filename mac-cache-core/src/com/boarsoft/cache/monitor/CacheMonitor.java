package com.boarsoft.cache.monitor;

import java.util.Map;

import com.boarsoft.cache.config.CacheDb;
import com.boarsoft.cache.config.CacheNode;
import com.boarsoft.cache.event.CacheEvent;

/**
 * 缓存监控接口
 * 
 * @author Mac_J
 *
 */
public interface CacheMonitor {
	/**
	 * 缓存监控组件内部的方法，由MyJedisShard在发现宕机节点时使用<br>
	 * 遍历所有客户端组，广播通知各组中的客户端切换并装载数据
	 * 
	 * @param dbCode
	 *            Cache DB code
	 * @param shardIndex
	 *            第N个分片
	 * @param to
	 *            要切换到的新的主节点
	 */
	void switch2(String dbCode, int index, CacheNode to);

	/**
	 * 
	 * @return
	 */
	Map<String, CacheDb> getDbMap();

	void onEvent(CacheEvent ce);
}
