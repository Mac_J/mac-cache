package com.boarsoft.cache.monitor;

/**
 * 
 * @author Mac_J
 *
 */
public interface CacheWarmer {
	/**
	 * 预热指定缓存DB的指定分片
	 * 
	 * @param db
	 *            缓存DB编号
	 * @param shardIndex
	 *            目标节点的hashKey
	 * @param group
	 *            缓存分组
	 */
	void warmUp(String dbCode, int shardIndex, String group);
}
