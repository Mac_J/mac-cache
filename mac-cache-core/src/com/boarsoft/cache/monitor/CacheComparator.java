package com.boarsoft.cache.monitor;

public interface CacheComparator {
	/**
	 * 找出两个节点中哪一个的数据最新
	 * 
	 * @param nextObj
	 *            next cache client resource object, eg: jedis
	 * @param currObj
	 *            current cache client resource object, eg: jedis
	 * @return 如果nextObj中的数据更新，返回1，相同返回0，更旧则返回-1
	 */
	int compare(Object nextObj, Object currObj);
}
