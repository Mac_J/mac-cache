package com.boarsoft.cache.monitor;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.ExecutorService;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.boarsoft.cache.CacheClient;
import com.boarsoft.cache.config.CacheDb;
import com.boarsoft.cache.config.CacheNode;
import com.boarsoft.cache.config.CacheShard;
import com.boarsoft.cache.event.CacheEvent;
import com.boarsoft.cache.event.CacheEventSvc;

/**
 * 切换后主备关系发生变化，不再与配置文件中的配置一致。 <br>
 * 这时，如果重启缓存监控，缓存监控会加载配置文件中的配置，导致缓存监控和缓存集群出现混乱。 <br>
 * 为解决这一问题，采用以下方案： <br>
 * 缓存监控重启后，遍历所有分片，检查各分片中各节点的状态，以更新从配置文件中加载的初始配置。 <br>
 * 
 * @author Mac_J
 *
 */
public class CacheMonitorImpl implements CacheMonitor, Runnable {
	private static final Logger log = LoggerFactory.getLogger(CacheMonitorImpl.class);

	/** */
	protected ExecutorService threadPool;
	/** 轮询检查所有缓存节点，每轮之间的时间间隔 */
	protected long interval = 6000L;
	/** */
	protected Map<String, CacheDb> dbMap;
	/** k: dbCode */
	protected Map<String, Set<CacheClient>> cacheClientMap;
	/** 缓存数据比较器 */
	protected CacheComparator comparator;
	/** */
	protected CacheEventSvc cacheEventSvc;

	@PostConstruct
	public void init() {
		// 遍历所有DB，调用使用这些DB的所有客户端分组
		if (cacheClientMap != null) {
			for (Entry<String, CacheDb> en : dbMap.entrySet()) {
				String dn = en.getKey();
				Set<CacheClient> ccSet = cacheClientMap.get(dn);
				if (ccSet != null) {
					for (CacheClient cc : ccSet) {
						this.coordinate(cc);
					}
				}
			}
		}
		threadPool.execute(this);
	}

	@SuppressWarnings("unchecked")
	protected void coordinate(CacheClient cc) {
		// 广播调用当前客户端分组中的所有客户端，让它们返回自身持有的主备信息，失败则不执行
		Map<String, Object> rm = null;
		try {
			rm = (Map<String, Object>) cc.getDbMap();
			log.debug("Collect {} clients db maps", rm.size());
		} catch (Exception e) {
			// No provider available for
			log.warn("Can not get db map from cache clients", e);
		}
		if (rm == null) {
			return;
		}
		Map<String, Map<String, CacheDb>> rm2 = new HashMap<String, Map<String, CacheDb>>();
		// 只保留没有问题的配置，以便后面检查
		for (Entry<String, Object> en : rm.entrySet()) {
			String addr = en.getKey();
			Object o = en.getValue();
			if (o == null) {
				log.warn("{} cacheClient.getDbMap() return null", addr);
				continue;
			}
			if (o instanceof Throwable) {
				log.error("{} cacheClient.getDbMap() throw exception", addr, (Throwable) o);
				continue;
			}
			if (o instanceof Map) {
				rm2.put(addr, (Map<String, CacheDb>) o);
				continue; // 如果当前节点返回的是map，则是正常的返回结果
			}
			log.error("{} cacheClient.getDbMap() return {}", addr, o);
		}
		// 如果采集到的客户端缓存配置与本地配置在数据量上不符，则抛出异常
		if (rm2.size() != rm.size()) {
			throw new IllegalStateException("Cache clients do not match my config");
		}
		// 如果采集到的有效客户端配置不为空，表示有活动的客户端，需要将其配置与本地配置比对
		if (!rm2.isEmpty()) {
			for (CacheDb db : dbMap.values()) {
				List<CacheShard> csLt = db.getShards();
				for (CacheShard cs : csLt) {
					this.coordinate(cc, rm2, cs);
					this.check(cs);
				}
			}
		}
	}

	/**
	 * 广播调用所有客户端，获取各客户端认知的主节点<br>
	 * 统计各节点的票数，并以此为优先级，依次检查这些节点是否有效<br>
	 * 将找到的得票最多且有效的节点确立为主节点
	 * 
	 * @param cc
	 *            CacheClient
	 * @param rm
	 *            remote cache DB maps
	 * @param cs
	 *            CacheShard
	 */
	protected void coordinate(CacheClient cc, Map<String, Map<String, CacheDb>> rm, CacheShard cs) {
		// 统计所有客户端所认知的主节点，保持添加顺序
		Map<CacheNode, Integer> cm = this.mapCandidates(rm, cs);
		// log.debug("mapCandidates return {}", JsonUtil.from(cm,
		// "jedisPool,redisNodeAgent"));
		// 根据得票数，将将候选人排序
		List<CacheNode> lt = this.listCandidates(cm);
		// log.debug("listCandidates return {}", JsonUtil.from(lt,
		// "jedisPool,redisNodeAgent"));
		// 遍历所有选项，找到可写，且缓存的数据版本最新（由插件决定）
		CacheNode nn = this.electMaster(cs, lt);
		// log.debug("electMaster return {}", JsonUtil.from(nn,
		// "jedisPool,redisNodeAgent"));
		// 如果有找到这样的节点就广播切换，否则保持默认配置，直到健康检查时再切换
		if (nn != null) {
			// 针对当前cacheClient组广播，要求该组的客户端切换主节点
			// 注：即使统计得到的主节点与当前配置的主节点要同，也需要广播
			// 因为有的节点所认知的主节点可能并不是与当前配置的主节点
			log.debug("Call switch2 {}", nn.getCode());
			// 总是执行warmUp动作，由warmer自行判断是否要装载数据
			if (cs.switch2(nn, cc)) {
				log.debug("Call warmUp {}/{}", cs.getDbCode(), cs.getIndex());
				// 让其中一个客户端执行数据装载（非广播方法）
				cc.warmUp(cs.getDbCode(), cs.getIndex());
			}
		}
	}

	/**
	 * 在各客户端的返回数据中收集所有候选人，并统计它们的得票数<br>
	 * 包括无票的配置文件中的节点（未被客户端认知为主节点的节点）<br>
	 * 先插入的是有票的候选人，后插入的是无票的候选人
	 * 
	 * @param rm
	 * @param cs
	 * @return
	 */
	protected Map<CacheNode, Integer> mapCandidates(Map<String, Map<String, CacheDb>> rm, CacheShard cs) {
		Map<CacheNode, Integer> cm = new LinkedHashMap<CacheNode, Integer>();
		for (Entry<String, Map<String, CacheDb>> en : rm.entrySet()) {
			Map<String, CacheDb> m = en.getValue();
			// 找到每个节点的对应缓存DB
			CacheDb d = m.get(cs.getDbCode());
			if (d == null) {
				continue;
			}
			// 找到指定的分片并返回其主节点
			CacheShard s = d.getShard(cs.getIndex());
			if (s == null) {
				continue;
			}
			CacheNode n = s.getMaster();
			if (cm.containsKey(n)) {
				cm.put(n, cm.get(n) + 1);
			} else {
				cm.put(n, 1);
			}
		}
		// 再按配置文件中的顺序添加余下节点
		if (!cm.containsKey(cs.getMaster())) {
			cm.put(cs.getMaster(), 0); // 票数为0
		}
		for (CacheNode bn : cs.getBackups().values()) {
			if (!cm.containsKey(bn)) {
				cm.put(bn, 0); // 票数为0
			}
		}
		return cm;
	}

	/**
	 * 根据得票数，将所有候选人排序，依序放入候选人列表
	 * 
	 * @param cm
	 * @return
	 */
	protected List<CacheNode> listCandidates(Map<CacheNode, Integer> cm) {
		List<CacheNode> lt = new LinkedList<CacheNode>();
		// 找出得票最多且有效的节点确立为主节点
		while (!cm.isEmpty()) {
			// 找出MAP中得票最多的节点
			CacheNode nn = null;
			int max = -1;
			for (Entry<CacheNode, Integer> en : cm.entrySet()) {
				CacheNode n = en.getKey();
				int v = en.getValue();
				if (v > max) {
					max = v;
					nn = n;
				}
			}
			cm.remove(nn); // 移除这个节点
			lt.add(nn); // 添加到队列
		}
		return lt;
	}

	protected CacheNode electMaster(CacheShard cs, List<CacheNode> lt) {
		if (lt.isEmpty()) {
			return null;
		}
		if (comparator == null) {
			return lt.get(0);
		}
		CacheNode nn = null;
		for (CacheNode n : lt) {
			if (n.test() == CacheNode.STATUS_WRITE) {
				if (nn == null) {
					nn = n;
					continue;
				}
				if (comparator.compare(n, nn) > 0) {
					nn = n; // 如果n中的数据更新
				}
			}
		}
		return nn;
	}

	public void check(CacheShard cs) {
		short ms = CacheNode.STATUS_WRITE;
		short bs = CacheNode.STATUS_WRITE;
		switch (cs.getStandby()) {
		case CacheShard.STANDBY_COLD:
			bs = CacheNode.STATUS_DOWN;
			break;
		case CacheShard.STANDBY_WARM:
			bs = CacheNode.STATUS_WRITE;
			break;
		case CacheShard.STANDBY_SLAVE:
			bs = CacheNode.STATUS_READ;
			break;
		case CacheShard.STANDBY_CLUSTER:
			return;
		}
		// 检查所有实例是否都可写，如果没有可用节点，或者某节点不可写，退出
		CacheNode cn = cs.getMaster();
		if (cn.test() != ms) {
			log.warn("Status of master cache node {} is {}, but {} be required"//
					, cn, cn.getStatus(), ms);
		}
		for (CacheNode bn : cs.getBackups().values()) {
			if (bn.test() != bs) {
				log.warn("Status of backup cache node {} is {}, but {} be required"//
						, cn, cn.getStatus(), ms);
			}
		}
	}

	@Override
	public void run() {
		// 遍历dbMap，检查所有CacheDB下的每一个分片
		while (true) {
			long t = System.currentTimeMillis();
			for (Entry<String, CacheDb> en : dbMap.entrySet()) {
				String dn = en.getKey();
				log.debug("Test cache db {}", dn);
				CacheDb cdc = en.getValue();
				if (cdc == null) {
					log.error("Cache db {} is null", dn);
				} else {
					for (CacheShard csc : cdc.getShards()) {
						try {
							if (csc.test()) {
								continue;
							}
							String msg = String.format("Cache db %s is down", dn);
							log.error(msg);
							// 如果主节点测试不成功，且选不出新主节点，则此缓存DB失败，产生告警信息
							this.onEvent(new CacheEvent(CacheEvent.DB_DOWN, cdc, msg));
						} catch (Exception e) {
							log.error("Error on test cache db {}", dn, e);
						}
					}
				}
			}
			t = System.currentTimeMillis() - t;
			if (t < interval) {
				try {
					Thread.sleep(interval - t);
				} catch (InterruptedException e) {
					log.error("Be interrupted while testing caches.", e);
				}
			}
		}

	}

	@Override
	public void onEvent(CacheEvent ce) {
		if (cacheEventSvc == null) {
			return;
		}
		cacheEventSvc.on(ce);
	}

	@Override
	public void switch2(String dbCode, int shardIndex, CacheNode to) {
		Set<CacheClient> ccSet = cacheClientMap.get(dbCode);
		if (ccSet == null || ccSet.isEmpty()) {
			log.warn("No cache clients set for cache db {}", dbCode);
			return;
		}
		for (CacheClient cc : ccSet) {
			cc.switch2(dbCode, shardIndex, to);
			cc.warmUp(dbCode, shardIndex);
		}
	}

	@Override
	public Map<String, CacheDb> getDbMap() {
		return dbMap;
	}

	public ExecutorService getThreadPool() {
		return threadPool;
	}

	public void setThreadPool(ExecutorService threadPool) {
		this.threadPool = threadPool;
	}

	public long getInterval() {
		return interval;
	}

	public void setInterval(long interval) {
		this.interval = interval;
	}

	public Map<String, Set<CacheClient>> getCacheClientMap() {
		return cacheClientMap;
	}

	public void setCacheClientMap(Map<String, Set<CacheClient>> cacheClientMap) {
		this.cacheClientMap = cacheClientMap;
	}

	public CacheComparator getComparator() {
		return comparator;
	}

	public void setComparator(CacheComparator comparator) {
		this.comparator = comparator;
	}

	public void setDbMap(Map<String, CacheDb> dbMap) {
		this.dbMap = dbMap;
	}

	public CacheEventSvc getCacheEventSvc() {
		return cacheEventSvc;
	}

	public void setCacheEventSvc(CacheEventSvc cacheEventSvc) {
		this.cacheEventSvc = cacheEventSvc;
	}
}
