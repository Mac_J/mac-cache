package com.boarsoft.cache.event;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component("cacheEventSvcMocker")
public class CacheEventSvcMocker implements CacheEventSvc {
	private static final Logger log = LoggerFactory.getLogger(CacheEventSvcMocker.class);

	@Override
	public void on(CacheEvent ce) {
		log.info("Received cache event: {} from {}", ce.getContent(), ce.getFrom());
	}

}
