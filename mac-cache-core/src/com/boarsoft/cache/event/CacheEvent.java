package com.boarsoft.cache.event;

import java.io.Serializable;

import com.boarsoft.cache.config.CacheDb;
import com.boarsoft.cache.config.CacheNode;
import com.boarsoft.common.util.InetUtil;

public class CacheEvent implements Serializable {
	private static final long serialVersionUID = -3551736446037908851L;

	public static final String DB_DOWN = "db.down";
	public static final String NODE_STATUS = "node.status";
	
	protected String code; 
	protected String key;
	protected String from;
	protected String dbCode;
	protected String content;

	public CacheEvent(String code, CacheDb db, String content) {
		this.code = code;
		this.key = db.getCode();
		this.from = InetUtil.getAddr();
		this.dbCode = db.getCode();
		this.content = content;
	}

	public CacheEvent(String code, String dbCode, CacheNode bn, String content) {
		this.code = code;
		this.key = bn.getCode();
		this.from = InetUtil.getAddr();
		this.dbCode = dbCode;
		this.content = content;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getFrom() {
		return from;
	}

	public void setFrom(String from) {
		this.from = from;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getDbCode() {
		return dbCode;
	}

	public void setDbCode(String dbCode) {
		this.dbCode = dbCode;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}
}
