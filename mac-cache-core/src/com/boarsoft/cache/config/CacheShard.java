package com.boarsoft.cache.config;

import java.io.Closeable;
import java.io.Serializable;
import java.util.Map;

import com.boarsoft.cache.CacheClient;

public interface CacheShard extends Serializable, Closeable {
	/** 备用节点准备（未）启动 */
	short STANDBY_COLD = 0;
	/** 备用节点运行中，但未装载（同步）数据 */
	short STANDBY_WARM = 1;
	/** 备用节点运行中，且已装载（同步）数据 */
	short STANDBY_SLAVE = 2;
	/** Cluster集群 */
	short STANDBY_CLUSTER = 3;

	/**
	 * 测试shard
	 * 
	 * @return
	 */
	boolean test();

	CacheNode getMaster();

	/**
	 * 返回主节点的（Jedis）
	 * 
	 * @return
	 */
	Object getResource();

	/**
	 * 返回主节点的（Jedis）
	 * 
	 * @return
	 */
	Object getResource(boolean readOnly);

	/**
	 * 
	 * @param dbCode
	 * @param index
	 */
	void init(String dbCode, int index);

	/**
	 * 在本地，将当前分片的主节点切换到新的节点
	 * 
	 * @param n1
	 */
	boolean switch2(CacheNode n1);

	/**
	 * 在本地，将当前分片的主节点切换到新的节点<br/>
	 * 并通知指定客户端分组中的客户端应用，将当前分片的主节点切换到新的节点
	 * 
	 * @param n1
	 * @param cc
	 *            CacheClient
	 * @return
	 */
	boolean switch2(CacheNode n1, CacheClient cc);

	short getStandby();

	Map<String, CacheNode> getBackups();

	int getIndex();

	void setIndex(int index);

	String getDbCode();

	short getStatus();
}