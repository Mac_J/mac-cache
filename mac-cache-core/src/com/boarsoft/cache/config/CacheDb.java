package com.boarsoft.cache.config;

import java.io.Closeable;
import java.util.List;
import java.util.Map;

/**
 * 
 * @author Mac_J
 *
 */
public interface CacheDb extends Closeable {
	/**
	 * 返回所有分片对象
	 * 
	 * @return
	 */
	List<CacheShard> getShards();

	void setCode(String dc);

	void init();

	String getCode();

	CacheShard getShard(int index);

	void setShards(List<CacheShard> lt);

	Object getResource(String key, boolean readOnly);
	
	Object getResource(String key);

	Map<String, Object> getResourceMap();
}
