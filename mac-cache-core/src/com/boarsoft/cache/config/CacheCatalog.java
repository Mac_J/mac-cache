package com.boarsoft.cache.config;

import java.util.List;
import java.util.Map;

/**
 * 用于通过本地或远程（CacheMonitor）配置，获取各缓存库及其分片信息
 * 
 * @author Mac_J
 *
 */
public interface CacheCatalog {

	Map<String, CacheDb> getDbMap();

	List<CacheShard> getShards(String dn);

	CacheNode getCacheNode(String dbCode, int shardIndex, String nodeCode);
}
