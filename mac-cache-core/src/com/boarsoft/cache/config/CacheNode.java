package com.boarsoft.cache.config;

public interface CacheNode {
	/** 无法连接，不可访问 */
	short STATUS_DOWN = 0;
	/** 可连接，可读 */
	short STATUS_READ = 1;
	/** 可连接，可写 */
	short STATUS_WRITE = 2;

	/**
	 * 返回节点是否可读
	 * 
	 * @return
	 */
	boolean isReadable();

	/**
	 * 返回节点是否可写
	 * 
	 * @return
	 */
	boolean isWriteable();

	/**
	 * 读取缓存中的数据版本号，以检查缓存是否可以被访问，数据是否已装载
	 * 
	 * @return
	 */
	short test();

	/**
	 * 启动指定节点，如果该节点还未启动的话
	 * 
	 * @return
	 */
	public boolean startUp();

	/**
	 * 停机，返回是否成功
	 * 
	 * @return
	 */
	public boolean shutdown();

	/**
	 * 清空当前分片中的数据，以释放内存
	 */
	void clearAll();

	String getHost();

	int getPort();

	String getCode();

	void close();

	void open();

	Object getResource();

	boolean toMaster();

	boolean toSlave(String master);

	/**
	 * 
	 * @return
	 */
	boolean isReadonly();

	short getStatus();
}
