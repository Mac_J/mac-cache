package com.boarsoft.cache.config;

import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.annotation.PostConstruct;

import com.boarsoft.common.Util;

/**
 * 缓存分库与分片配置，可以直接来源于本地配置或从远程获取
 * 
 * @author Mac_J
 *
 */
public class CacheCatalogImpl implements CacheCatalog {
	/** */
	protected Map<String, CacheDb> dbMap;

	@PostConstruct
	public void init() {
		for (Entry<String, CacheDb> en : dbMap.entrySet()) {
			String dn = en.getKey();
			CacheDb db = en.getValue();
			if (db == null) {
				throw new IllegalStateException(String.format(//
						"MyJedisDb %s can not be null", dn));
			}
			db.setCode(dn);
			db.init();
		}
	}

	@Override
	public CacheNode getCacheNode(String dbCode, int shardIndex, String nodeCode) {
		List<CacheShard> lt = this.getShards(dbCode);
		if (lt == null || lt.isEmpty()) {
			return null;
		}
		CacheShard cs = lt.get(shardIndex);
		CacheNode cn = cs.getBackups().get(nodeCode);
		if (cn == null) {
			cn = cs.getMaster();
			if (cn.getCode().equals(nodeCode)) {
				return cn;
			}
			return null;
		}
		return cn;
	}

	@Override
	public List<CacheShard> getShards(String dn) {
		CacheDb cdb = dbMap.get(dn);
		if (cdb == null) {
			return null;
		}
		return cdb.getShards();
	}

	@Override
	public Map<String, CacheDb> getDbMap() {
		return dbMap;
	}

	public void setDbMap(Map<String, CacheDb> dbMap) {
		this.dbMap = dbMap;
		if (dbMap == null) {
			return;
		}
		for (Entry<String, CacheDb> en : dbMap.entrySet()) {
			String k = en.getKey();
			CacheDb db = en.getValue();
			if (db == null) {
				continue;
			}
			if (Util.strIsEmpty(db.getCode())) {
				db.setCode(k);
			}
		}
	}
}
