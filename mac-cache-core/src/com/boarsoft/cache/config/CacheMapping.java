package com.boarsoft.cache.config;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.annotation.PostConstruct;

import com.boarsoft.common.Util;

/**
 * 连接池与分组配置
 * 
 * @author Mac_J
 *
 */
public class CacheMapping {
	/** */
	protected String defaultDbName = "default";
	/** Spring注入，{ k: group, v: db（poolName） } */
	protected Map<String, String> groupMap;

	/** 非注入，根据groupMap初始化，{ k: db, v: groups } */
	protected Map<String, Set<String>> dbMap = new HashMap<String, Set<String>>();

	@PostConstruct
	public void init() {
		dbMap.clear();
		for (Entry<String, String> en : groupMap.entrySet()) {
			String g = en.getKey();
			String db = en.getValue();
			if (dbMap.containsKey(db)) {
				dbMap.get(db).add(g);
			} else {
				Set<String> s = new HashSet<String>();
				s.add(g);
				dbMap.put(db, s);
			}
		}
	}

	/**
	 * 返回指定分组所在的逻辑“库”名<br>
	 * 找不到匹配的数据就返回默认的逻辑“库”名
	 * 
	 * @param group
	 * @return
	 */
	public String getDbByGroup(String group) {
		if (Util.strIsEmpty(group) || !groupMap.containsKey(group)) {
			return defaultDbName;
		}
		return groupMap.get(group);
	}

	public Set<String> getPools() {
		return dbMap.keySet();
	}

	public Map<String, String> getGroupMap() {
		return groupMap;
	}

	public void setGroupMap(Map<String, String> groupMap) {
		this.groupMap = groupMap;
	}

	public Map<String, Set<String>> getDbMap() {
		return dbMap;
	}

	public void setDbMap(Map<String, Set<String>> dbMap) {
		this.dbMap = dbMap;
	}

	public String getDefaultDbName() {
		return defaultDbName;
	}

	public void setDefaultDbName(String defaultDbName) {
		this.defaultDbName = defaultDbName;
	}
}
