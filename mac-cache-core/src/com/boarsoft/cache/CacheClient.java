package com.boarsoft.cache;

import java.io.Closeable;
import java.util.Map;
import java.util.Set;

import com.boarsoft.cache.config.CacheDb;
import com.boarsoft.cache.config.CacheNode;

/**
 * 此接口并非缓存服务器的客户端，而是缓存中间件的客户端
 * 
 * @author Mac_J
 *
 */
public interface CacheClient {
	/**
	 * 广播方法，用于接收来自监控节点的通知<br/>
	 * 将指定db的对应shard的master切换为tn<br/>
	 * 表示n0宕机，并已由n1替代之<br/>
	 * 注：不应在此方法中执行warmUp
	 * 
	 * @param dbCode
	 * @param shardIndex
	 * @param to
	 *            替补节点
	 * @return
	 */
	Object switch2(String dbCode, int shardIndex, CacheNode to);

	/**
	 * 由CacheMonitor上的CacheShard，通过RPC调用当前客户端（应用）<br>
	 * 根据装载数据到指定节点（备用节点），非广播方法
	 * 
	 * @param dbCode
	 *            故障节点和备用节点所属的缓存DB
	 * @param shardIndex
	 *            分片索引
	 * @return
	 */
	boolean warmUp(String dbCode, int shardIndex);

	/**
	 * 用于向监控返回dbMap
	 * 
	 * @return
	 */
	Object getDbMap();

	CacheDb getDbByGroup(String group);

	CacheDb getDbByName(String dbName);

	void closeAll();

	void close(String dbName);

	void close(Closeable obj);

	Map<String, Set<String>> getDbMaping();

	Map<String, String> getGroupMap();
}
