package com.boarsoft.cache.jvmm;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.boarsoft.cache.api.SourceDataService;

/**
 * 以推拉结合的方式同步各缓存分组的版本号。 监听当缓存版本号更新事件，从远程数据服务查询最新数据并缓存起来
 * 
 * @author Mac_J
 *
 */
public class CacheDataSyncImpl implements Runnable {
	private static final Logger log = LoggerFactory.getLogger(CacheDataSyncImpl.class);

	/** 缓存来源数据查询服务 */
	protected Map<String, SourceDataService> srcDataSvcMap = new HashMap<String, SourceDataService>();
	/** */
	protected Map<String, String> versionMap;

	protected Map<String, Map<String, Object>> cacheMap;

	@PostConstruct
	public void init() {
		// 初始时，根据配置的缓存数据版本号，到各源数据服务提供者查询数据并缓存
		for (Entry<String, SourceDataService> en : srcDataSvcMap.entrySet()) {
			String group = en.getKey();
			String ver = versionMap.get(group);
			SourceDataService sds = srcDataSvcMap.get(group);
			try {
				cacheMap.put(group, sds.query(group, ver));
			} catch (Exception e) {
				log.warn("Error on query data for cache {}", group, e);
			}
		}
	}
	
	@Override
	public void run(){
		// 通过长轮询从配置中心查询各缓存的数据版本号
		
	}

	public Map<String, SourceDataService> getSrcDataSvcMap() {
		return srcDataSvcMap;
	}

	public void setSrcDataSvcMap(Map<String, SourceDataService> srcDataSvcMap) {
		this.srcDataSvcMap = srcDataSvcMap;
	}

	public Map<String, String> getVersionMap() {
		return versionMap;
	}

	public void setVersionMap(Map<String, String> versionMap) {
		this.versionMap = versionMap;
	}
}