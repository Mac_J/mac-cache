package com.boarsoft.cache.jvmm;

import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;

import javax.annotation.PostConstruct;

import com.boarsoft.cache.Cache;
import com.boarsoft.common.util.BeanUtil;

public class JvmmCacheImpl implements Cache, Runnable {
	/** */
	protected Map<String, Map<String, CachedObject>> dataMap = //
			new ConcurrentHashMap<String, Map<String, CachedObject>>();

	protected ScheduledExecutorService scheduler;

	@PostConstruct
	public void init() {
		scheduler.scheduleWithFixedDelay(this, 0, 3, TimeUnit.SECONDS);
	}

	@Override
	public void run() {
		// 移除过期的数据
		long now = System.currentTimeMillis();
		for (Entry<String, Map<String, CachedObject>> en : dataMap.entrySet()) {
			Map<String, CachedObject> gm = en.getValue();
			for (String key : gm.keySet()) {
				CachedObject o = gm.get(key);
				if (o == null) {
					continue;
				}
				if (o.getExpire() < now) {
					gm.remove(key);
				}
			}
		}
	}

	@Override
	public long incr(String group, String key, long delta) {
		AtomicLong ai = (AtomicLong) this.get(group, key);
		if (ai == null) {
			synchronized (dataMap) {
				ai = (AtomicLong) this.get(group, key);
				if (ai == null) {
					this.put(group, key, new AtomicLong(delta));
					return delta;
				}
			}
		}
		return ai.addAndGet(delta);
	}

	@Override
	public Object get(String group, String key) {
		Map<String, CachedObject> m = this.getMap(group);
		CachedObject o = m.get(key);
		if (o == null) {
			return null;
		}
		if (o.getExpire() < System.currentTimeMillis()) {
			m.remove(key);
			return null;
		}
		return o.getData();
	}

	@Override
	public void put(String group, String key, Object value) {
		this.getMap(group).put(key, new CachedObject(value));
	}

	@Override
	public void clear() {
		dataMap.clear();
	}

	@Override
	public void remove(final String group) {
		dataMap.remove(group);
	}

	@Override
	public void remove(final String group, final String key) {
		this.getMap(group).remove(key);
	}

	@Override
	public Map<String, Object> list(String group) {
		long now = System.currentTimeMillis();
		Map<String, CachedObject> cm = this.getMap(group);
		Map<String, Object> rm = new HashMap<String, Object>();
		for (Entry<String, CachedObject> en : cm.entrySet()) {
			String k = en.getKey();
			CachedObject o = cm.get(k);
			if (o.before(now)) {
				cm.remove(k);
			} else {
				rm.put(k, o.getData());
			}
		}
		return rm;
	}

	@Override
	public Map<String, Object> list(String dbName, String group) {
		return this.list(group);
	}

	@Override
	public void get(String group, String key, Map<String, Object> map) throws Exception {
		CachedObject o = this.getMap(group).get(key);
		Object d = o.getData();
		if (d == null) {
			return;
		}
		BeanUtil.fillMapWithObject(map, d);
	}

	@Override
	public void set(String group, String key, Map<String, Object> map) throws Exception {
		CachedObject o = this.getMap(group).get(key);
		Object d = o.getData();
		if (d == null) {
			return;
		}
		BeanUtil.fillObjectWithMap(d, map);
	}

	@Override
	public boolean exists(String group, String key) throws UnsupportedEncodingException {
		Map<String, CachedObject> m = this.getMap(group);
		CachedObject o = m.get(key);
		if (o == null) {
			return false;
		}
		if (o.before(System.currentTimeMillis())) {
			m.remove(key);
			return false;
		}
		return true;
	}

	protected Map<String, CachedObject> getMap(String group) {
		Map<String, CachedObject> rm = dataMap.get(group);
		if (rm == null) {
			synchronized (dataMap) {
				rm = dataMap.get(group);
				if (rm == null) {
					rm = new ConcurrentHashMap<String, CachedObject>();
					dataMap.put(group, rm);
				}
			}
		}
		return rm;
	}

	public Map<String, Map<String, CachedObject>> getDataMap() {
		return dataMap;
	}

	public void setDataMap(Map<String, Map<String, CachedObject>> dataMap) {
		this.dataMap = dataMap;
	}

	@Override
	public void put(String group, String key, Object value, int expire) throws Exception {
		this.getMap(group).put(key, new CachedObject(value, System.currentTimeMillis(), expire));
	}

	@Override
	public void set(String group, String key, Map<String, Object> om, int expire) throws Exception {
		this.set(group, key, om);
		this.expire(group, key, expire);
	}

	@Override
	public Map<String, Set<String>> getDbMaping() {
		// 本地缓存不实现此方法
		// Map<String, Set<String>> m = new HashMap<String, Set<String>>();
		// m.put("default", dataMap.keySet());
		// return m;
		return null;
	}

	@Override
	public String str(String group, String key) {
		CachedObject o = this.getMap(group).get(key);
		return o == null ? null : (String) o.getData();
	}

	@Override
	public void str(String group, String key, String value, int expire) {
		if (expire > 0) {
			expire += System.currentTimeMillis();
		}
		dataMap.get(group).put(key, new CachedObject(value, System.currentTimeMillis(), expire));
	}

	@Override
	public void str(String group, String key, String value) {
		dataMap.get(group).put(key, new CachedObject(value));
	}

	@Override
	public double incr(String group, String key, double delta, int scale) {
		BigDecimal bd = new BigDecimal(delta).scaleByPowerOfTen(scale);
		AtomicLong ai = (AtomicLong) this.get(group, key);
		if (ai == null) {
			synchronized (dataMap) {
				ai = (AtomicLong) this.get(group, key);
				if (ai == null) {
					this.put(group, key, new AtomicLong(bd.longValue()));
					return delta;
				}
			}
		}
		long l = ai.addAndGet(bd.longValue());
		return new BigDecimal(l).scaleByPowerOfTen(-scale).doubleValue();
	}

	@Override
	public void num(String group, String key, int value) {
		dataMap.get(group).put(key, new CachedObject(value));
	}

	@Override
	public void num(String group, String key, double value) {
		dataMap.get(group).put(key, new CachedObject(value));
	}

	@Override
	public long ttl(String group, String key) {
		CachedObject o = dataMap.get(group).get(key);
		if (o == null) {
			throw new IllegalStateException(String.format(//
					"Cache data %s/%s does not exist", group, key));
		}
		return o.getExpire();
	}

	@Override
	public void expire(String group, String key, int expire) {
		CachedObject o = this.getMap(group).get(key);
		if (o == null) {
			throw new IllegalStateException(String.format(//
					"Cache data %s/%s does not exist", group, key));
		}
		if (expire > 0) {
			expire += System.currentTimeMillis();
		}
		o.setExpire(expire);
	}

	@Override
	public boolean setnx(String group, String key, String value, int expire) {
		Map<String, CachedObject> m = this.getMap(group);
		synchronized (m) {
			if (m.containsKey(key)) {
				return false;
			}
			m.put(key, new CachedObject(value, System.currentTimeMillis(), expire));
		}
		return true;
	}

	public ScheduledExecutorService getScheduler() {
		return scheduler;
	}

	public void setScheduler(ScheduledExecutorService scheduler) {
		this.scheduler = scheduler;
	}
}
