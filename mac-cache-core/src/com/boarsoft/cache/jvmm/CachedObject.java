package com.boarsoft.cache.jvmm;

import java.io.Serializable;

public class CachedObject implements Serializable {
	private static final long serialVersionUID = -1113646454723885867L;

	protected Object data;

	protected long expire = Long.MAX_VALUE;

	public CachedObject() {

	}

	public CachedObject(Object data) {
		this.data = data;
	}

	public CachedObject(Object data, long now, int expire) {
		this.data = data;
		this.expire = (expire > 0 ? now + expire : now);
	}

	public boolean before(long now) {
		return expire < now;
	}

	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}

	public long getExpire() {
		return expire;
	}

	public void setExpire(long expire) {
		this.expire = expire > 0 ? expire : System.currentTimeMillis();
	}
}
