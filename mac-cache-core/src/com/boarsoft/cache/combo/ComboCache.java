package com.boarsoft.cache.combo;

import java.io.UnsupportedEncodingException;
import java.util.Map;

import com.boarsoft.cache.Cache;

public interface ComboCache extends Cache {
	/**
	 * 将数据先后写入cache1（本地缓存）和cache2（远程缓存）的指定分组
	 * 
	 * @param group
	 * @param key
	 * @param value
	 * @throws Exception
	 */
	void put2(String group, String key, Object value) throws Exception;

	/**
	 * 从cache1（本地缓存）的指定分组读取数据，如果没有命中，再从cache2（远程缓存）的指定分组中读取数据
	 * 
	 * @param group
	 * @param key
	 * @return
	 * @throws Exception
	 */
	Object get2(String group, String key) throws Exception;

	/**
	 * 先后清除cache1（本地缓存）和cache2（远程缓存）的指定分组中的数据
	 * 
	 * @param group
	 */
	void clear2(String group);

	/**
	 * 先后移除cache1（本地缓存）和cache2（远程缓存）中指定的分组
	 * 
	 * @param group
	 */
	void remove2(String group);

	/**
	 * 先后移除cache1（本地缓存）和cache2（远程缓存）中指定的分组下的指定键值
	 * 
	 * @param group
	 * @param key
	 * @throws UnsupportedEncodingException 
	 */
	void remove2(String group, String key) throws UnsupportedEncodingException;

	/**
	 * 装载cache2的数据到cache1（经转换）<br>
	 * 注意：此方法不支持分片数据的装载
	 * 
	 * @throws Exception
	 * 
	 */
	void load() throws Exception;

	/**
	 * 
	 * @param group
	 * @param key
	 * @param delta
	 * @return
	 * @throws Exception
	 */
	long incr2(String group, String key, long delta) throws Exception;

	Cache getCache1();

	Cache getCache2();

	void set2(String group, String key, Map<String, Object> om) throws Exception;

	void get2(String group, String key, Map<String, Object> om) throws Exception;

	boolean exists2(String group, String key) throws UnsupportedEncodingException;

	void clear2();

	double incr2(String group, String key, double delta, int scale);

	void str2(String group, String key, String value, int expire);

	void str2(String group, String key, String value);

	String str2(String group, String key, int expire);
}
