package com.boarsoft.cache.serialize;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.ServiceLoader;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.boarsoft.cache.mapper.ObjectMapper;
import com.boarsoft.cache.mapper.SimpleObjectMapper;
import com.boarsoft.serialize.JavaSerializer;
import com.boarsoft.serialize.ObjectSerializer;

public class CacheSerializer {
	private static final Logger log = LoggerFactory.getLogger(CacheSerializer.class);

	/** */
	protected String charset = "UTF-8";
	/** _C */
	protected byte[] classKey = new byte[] { 95, 67 };
	/** _V */
	protected byte[] valueKey = new byte[] { 95, 86 };

	/** 指定类型的对象需要用hashMap方式存储 */
	protected Set<String> types = new HashSet<String>();
	/** 实现对象与Map<String, Object>之间的相互转换 */
	protected ObjectMapper mapper = new SimpleObjectMapper();

	/** */
	public static ObjectSerializer serializer = new JavaSerializer();

	static {
		ServiceLoader<ObjectSerializer> sl = ServiceLoader.load(ObjectSerializer.class);
		Iterator<ObjectSerializer> it = sl.iterator();
		if (it.hasNext()) {
			serializer = it.next();
		}
	}

	public byte[] getBytes(String sk) throws UnsupportedEncodingException {
		return sk.getBytes(charset);
	}

	public String getKey(String group, String key) {
		return new StringBuilder(group).append(".").append(key).toString();
	}

	public Map<byte[], byte[]> encodeObj(Object ov) throws Exception {
		Map<String, Object> om = mapper.obj2map(ov);
		Map<byte[], byte[]> bm = new HashMap<byte[], byte[]>();
		for (Entry<String, Object> en : om.entrySet()) {
			String sk = en.getKey();
			log.debug("Encode {} of {}", sk, ov);
			Object f = om.get(sk);
			// byte[] bk = serializer.serialize(sk);
			byte[] bk = sk.getBytes(charset);
			byte[] bv = serializer.serialize(f);
			bm.put(bk, bv);
		}
		bm.put(classKey, ov.getClass().getName().getBytes(charset));
		return bm;
	}

	public Object decodeObj(String key, String className, Map<byte[], byte[]> byteMap)
			throws ClassNotFoundException, IOException, InstantiationException, IllegalAccessException {
		Map<String, Object> om = new HashMap<String, Object>();
		for (Entry<byte[], byte[]> en : byteMap.entrySet()) {
			byte[] bk = en.getKey();
			byte[] bv = en.getValue();
			// String sk = (String) serializer.deserialize(bk);
			String sk = new String(bk, charset);
			log.debug("Decode {} of {}/{}", sk, key, className);
			Object ov = serializer.deserialize(bv);
			om.put(sk, ov);
		}
		return mapper.map2obj(key, className, om);
	}

	public static byte[] serialize(Object ov) throws IOException {
		return serializer.serialize(ov);
	}

	public static Object deserialize(byte[] bv) throws ClassNotFoundException, IOException {
		return serializer.deserialize(bv);
	}

	public String getCharset() {
		return charset;
	}

	public void setCharset(String charset) {
		this.charset = charset;
	}

	public Set<String> getTypes() {
		return types;
	}

	public void setTypes(Set<String> types) {
		this.types = types;
	}

	public ObjectMapper getMapper() {
		return mapper;
	}

	public void setMapper(ObjectMapper mapper) {
		this.mapper = mapper;
	}

	public static ObjectSerializer getSerializer() {
		return serializer;
	}

	public static void setSerializer(ObjectSerializer serializer) {
		CacheSerializer.serializer = serializer;
	}

	public byte[] getClassKey() {
		return classKey;
	}

	public void setClassKey(byte[] classKey) {
		this.classKey = classKey;
	}

	public byte[] getValueKey() {
		return valueKey;
	}

	public void setValueKey(byte[] valueKey) {
		this.valueKey = valueKey;
	}
}
