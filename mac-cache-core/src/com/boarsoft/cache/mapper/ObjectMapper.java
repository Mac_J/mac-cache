package com.boarsoft.cache.mapper;

import java.util.Map;

public interface ObjectMapper {
	/**
	 * object 转 map
	 * 
	 * @param ov
	 * @return
	 */
	Map<String, Object> obj2map(Object ov);

	/**
	 * map 转 object
	 * 
	 * @param key
	 * @param className
	 * @param objMap
	 * @return
	 * @throws ClassNotFoundException
	 * @throws IllegalAccessException
	 * @throws InstantiationException
	 * @throws Exception
	 */
	Object map2obj(String key, String className, Map<String, Object> objMap)
			throws InstantiationException, IllegalAccessException, ClassNotFoundException;
}