package com.boarsoft.cache.mapper;

import java.util.HashMap;
import java.util.Map;

import com.boarsoft.common.util.BeanUtil;

public class SimpleObjectMapper implements ObjectMapper {
	@Override
	public Map<String, Object> obj2map(Object ov) {
		Map<String, Object> om = new HashMap<String, Object>();
		BeanUtil.fillMapWithObject(om, ov);
		return om;
	}

	@Override
	public Object map2obj(String key, String className, Map<String, Object> objMap) throws InstantiationException, IllegalAccessException, ClassNotFoundException {
		Object o = Class.forName(className).newInstance();
		BeanUtil.fillObjectWithMap(o, objMap);
		return o;
	}
}
