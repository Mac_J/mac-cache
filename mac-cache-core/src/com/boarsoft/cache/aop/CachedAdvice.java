package com.boarsoft.cache.aop;

import java.lang.reflect.Method;
import java.util.concurrent.atomic.AtomicInteger;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.boarsoft.cache.Cache;
import com.boarsoft.common.Util;

/**
 * 缓存AOP切面
 * 
 * @author Mac_J
 *
 */
@Aspect
public class CachedAdvice {
	private static final Logger log = LoggerFactory.getLogger(CachedAdvice.class);
	/** */
	protected Cache cache;

	protected int ratio = 1000;

	protected static final AtomicInteger count = new AtomicInteger(0);

	// @Around("@annotation(org.springframework.web.bind.annotation.RequestMapping)&&@annotation(com.boarsoft.common.Authorized)")
	@Around("@annotation(com.boarsoft.cache.aop.Cached)")
	public Object cache(ProceedingJoinPoint jp) throws Throwable {
		if (cache == null) {
			return jp.proceed();
		}
		MethodSignature signature = (MethodSignature) jp.getSignature();
		Method method = signature.getMethod();// jp.getArgs()[0];
		Cached a = method.getAnnotation(Cached.class);
		String group = a.group();
		String key = a.key();
		if (Util.strIsEmpty(key)) {
			Object[] aa = jp.getArgs();
			Object k = aa[a.index()];
			key = String.valueOf(k);
		}
		Object o = null;
		if (count.get() > 0) {
			if (count.incrementAndGet() < ratio) {
				return jp.proceed();
			}
			count.set(0);
		}
		try {
			o = cache.get(group, key);
		} catch (Exception e) {
			log.error("Can not get {}.{} from cache", group, key, e);
			count.set(1);
			return jp.proceed();
		}
		if (o == null) {
			log.warn("Cache result at {}.{}", group, key);
			o = jp.proceed();
			cache.put(group, key, o, a.expire());
		}
		return o;
	}

	public Cache getCache() {
		return cache;
	}

	public void setCache(Cache cache) {
		this.cache = cache;
	}

	public int getRatio() {
		return ratio;
	}

	public void setRatio(int ratio) {
		this.ratio = ratio;
	}
}
