package com.boarsoft.cache.aop;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 
 * @author Mac_J
 *
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface Cached {
	/**
	 * 缓存分组
	 * 
	 * @return
	 */
	public String group() default "default";

	/**
	 * 缓存键
	 * 
	 * @return
	 */
	public String key() default "";

	/**
	 * 失效时间
	 * 
	 * @return
	 */
	public int expire() default -1;

	/**
	 * 将第几个参数作为列
	 * 
	 * @return
	 */
	int index() default 0;
}