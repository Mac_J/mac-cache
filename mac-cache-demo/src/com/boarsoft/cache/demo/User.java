package com.boarsoft.cache.demo;

import java.io.IOException;
import java.io.Serializable;

public class User implements Serializable {
	private static final long serialVersionUID = -6569043791710513624L;

	protected String name;
	protected int age;

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public static void main(String[] args) throws IOException {
	}
}
