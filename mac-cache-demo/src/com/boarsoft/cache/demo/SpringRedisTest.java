package com.boarsoft.cache.demo;

import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Component;

@Component("springRedisTest")
public class SpringRedisTest {
	@Cacheable(cacheManager = "redisCacheManager", cacheNames = { "default" })
	public String get(String key) {
//		System.out.println("Getting data ".concat(key));
		return "hello";
	}
	
	@CachePut(cacheManager = "redisCacheManager", cacheNames = { "default" })
	public String put(String key) {
//		System.out.println("Getting data ".concat(key));
		return "hello";
	}
}
