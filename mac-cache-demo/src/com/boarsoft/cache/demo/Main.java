package com.boarsoft.cache.demo;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cache.CacheManager;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.boarsoft.cache.Cache;
import com.boarsoft.cache.CacheClient;
import com.boarsoft.cache.combo.ComboCache;
import com.boarsoft.cache.config.CacheDb;

import redis.clients.jedis.Jedis;

public class Main {
	private static Logger log = LoggerFactory.getLogger(Main.class);
	private static ClassPathXmlApplicationContext ctx;

	private static volatile AtomicLong totalCount = new AtomicLong(0);
	private static final ExecutorService pool = Executors.newFixedThreadPool(20);
	private static final ScheduledExecutorService executor = Executors.newScheduledThreadPool(1);
	private static long start = 0L;

	public static void main(String[] args) throws Exception {
		ctx = new ClassPathXmlApplicationContext("classpath:conf/context.xml");
		System.out.println("Startup ".concat(ctx.isRunning() ? "successfully." : "failed."));
		CaffeineTest ct = ctx.getBean("caffeineTest", CaffeineTest.class);
		System.out.println(ct.get("hi"));
		// Thread.sleep(6000);
		// System.out.println(ct.get("hi"));
		// System.out.println(ct.get("hi"));

		SpringRedisTest srt = ctx.getBean("springRedisTest", SpringRedisTest.class);
		System.out.println(srt.get("hi"));
		// Thread.sleep(6000);
		// System.out.println(srt.get("hi"));
		// System.out.println(srt.get("hi"));

		Cache cache = (Cache) ctx.getBean("redisCache");
		testCache(cache);
		// testSharding(cache);
		// CacheClient client = (CacheClient) ctx.getBean("redisClient");
		// testJedisSharding(client);
		// testComboCache((ComboCache) ctx.getBean("comboCache"));
		cache.put("test", "hello", "hello");
		System.out.println(cache.get("test", "hello"));

		CacheManager cm = (CacheManager) ctx.getBean("redisCacheManager");
		cm.getCache("default").put("hello", "hello");

		executor.scheduleAtFixedRate(new Runnable() {
			@Override
			public void run() {
				if (start == 0) {
					start = System.currentTimeMillis();
					totalCount.set(0L);
					return;
				}
				System.out.println("cur count: " + totalCount.get());
				System.out.println("cur minute: " + ((System.currentTimeMillis() - start) / 1000));
				System.out.println("cur tps: " + totalCount.get() / ((System.currentTimeMillis() - start) / 1000));
			}
		}, 10, 5, TimeUnit.SECONDS);

		for (int i = 0; i < 10; i++) {
			pool.submit(new Runnable() {
				@Override
				public void run() {
					while (true) {
						try {
							// for (int j=0; j<10; j++) {
							// srt.put("hello");
							cm.getCache("default").put("hello", "hello");
							// // cache.put("base", "hello", "hello");
							// cache.put("test", "hello", "hello");
							// System.out.println(cache.get("test", "hello"));
							// }
						} catch (Exception e) {
							e.printStackTrace();
						}
						totalCount.addAndGet(1);
					}
				}
			});
		}

		// System.out.println(srt.get("hi"));
		// System.out.println(srt.get("hi"));
		//
		//// Cache cache = (Cache) ctx.getBean("redisCache");
		//// testCache(cache);
		//// testSharding(cache);
		//// CacheClient client = (CacheClient) ctx.getBean("redisClient");
		//// testJedisSharding(client);
		//// testComboCache((ComboCache) ctx.getBean("comboCache"));
		// } finally {
		// ctx.close();
		// }
	}

	public static void testJedisSharding(CacheClient client) {
		CacheDb db = client.getDbByName("default");
		// 集群为 Redis Cluster 时的用法
		// ClusterJedisDb cd = (ClusterJedisDb) db;
		// JedisCluster jc = cd.getJedisCluster();
		// jc.set(k, v);
		// 集群为 主从/主备 时的用法
		for (int i = 0; i < 10; i++) {
			String v = String.valueOf(i);
			String k = "J".concat(v);

			Jedis jedis = (Jedis) db.getResource(k);
			try {
				jedis.set(k, v);
				log.info("Put {} to {}", jedis.get(k), jedis.getClient().getPort());
			} finally {
				jedis.close();
			}
		}
	}

	public static void testSharding(Cache cache) {
		for (int i = 0; i < 10; i++) {
			String v = String.valueOf(i);
			String k = "C".concat(v);
			try {
				cache.put("user", k, v);
				log.info("Put {} to cache", cache.get("user", k));
			} catch (Exception e) {
				log.error("Error on put cache {}", k, e);
			}
		}
	}

	public static void testCache(Cache cache) throws Exception {
		// 清空缓存
		cache.clear();
		// 将字符串“v1”放到conf组的k1键上
		cache.put("conf", "k1", "v1");
		cache.put("conf", "k2", "v2");
		// 将对象u放到conf组的u1键上
		User u = new User();
		u.setAge(38);
		u.setName("Mac_J");
		cache.put("conf", "u1", u);
		// 读取conf下的k1
		String v1 = (String) cache.get("conf", "k1");
		log.info("conf.k1={}", v1);
		// 读取conf下的u1
		User u1 = (User) cache.get("conf", "u1");
		log.info("conf.u1={}", u1);
	}

	public static void testComboCache(ComboCache cc) throws Exception {
		// 手动将loadMap中配置数据组从cache2装载到cache1
		cc.load();
		String v2 = (String) cc.get("conf", "k2");
		log.info("conf.k2={}", v2);
		// 直接将键值对写入cache2的conf组
		cc.put2("conf", "k3", "v3");
		// 尝试直接读取刚才放入的值
		log.info("conf.k3={}", (String) cc.get2("conf", "k3"));
		// 尝试从cache1读取刚才放入到cache2的值
		log.info("conf.k3={}", (String) cc.get("conf", "k3"));
		cc.clear2();
	}
}
