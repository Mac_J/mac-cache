package com.boarsoft.cache.demo;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Component;

@Component("caffeineTest")
public class CaffeineTest {
	@Cacheable({ "test" })
	public String get(String key) {
		System.out.println("Getting data ".concat(key));
		return "hello";
	}
}
