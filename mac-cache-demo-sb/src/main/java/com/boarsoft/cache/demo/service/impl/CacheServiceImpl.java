package com.boarsoft.cache.demo.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.boarsoft.cache.demo.model.TestModel;
import com.boarsoft.cache.demo.service.CacheService;
import com.google.common.hash.BloomFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpSession;
import java.time.LocalDateTime;

/**
 * @Description:
 * @Author: 刘超
 * @Date: 2023-03-01 9:58
 */
@Service
public class CacheServiceImpl implements CacheService {

    @Autowired
    @Qualifier("testModelBloom")
    BloomFilter<String> testModelBloom;

    @Override
    @Cacheable(cacheManager = "redisCacheManager", value = "default", key = "#id", condition = "#id<3", unless = "#result==null")
    public TestModel getData(int id) {
        System.out.println("造数据..." + id + " " + LocalDateTime.now());
        if (0 == id) {
            return null;
        }
        TestModel testModel = new TestModel();
        testModel.setMsg(String.valueOf(id));
        testModelBloom.put(testModel.getMsg());
        return testModel;
    }

    @Override
    @Cacheable(cacheManager = "redisCacheManager", value = "default", key = "#id", condition = "#id<3", unless = "#result==null")
    public String getSession(int id, HttpSession session) {
        System.out.println(JSONObject.toJSONString(session));
        return JSONObject.toJSONString(session);
    }


}
