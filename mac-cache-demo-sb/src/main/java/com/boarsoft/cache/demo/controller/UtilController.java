package com.boarsoft.cache.demo.controller;

import com.boarsoft.cache.spring.util.RedisUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Description:
 * @Author: 刘超
 * @Date: 2023-07-19 17:14
 */
@RestController
@RequestMapping("/util")
public class UtilController {

    private static Logger logger = LoggerFactory.getLogger(UtilController.class);
    @RequestMapping("/put")
    public String put(@RequestParam("key") String key,@RequestParam("value") String value) {
        RedisUtil.set(key,value);
        return "ok";
    }

    @RequestMapping("/get")
    public String get(@RequestParam("key") String key){
        String result = String.valueOf(RedisUtil.get(key));
        logger.info("redisUtil get key 【"+key+"】 value is : "+result);
        return result;
    }
    @RequestMapping("/del")
    public String del(@RequestParam("key") String key) {
        RedisUtil.delete(key);
        return "ok";
    }

}
