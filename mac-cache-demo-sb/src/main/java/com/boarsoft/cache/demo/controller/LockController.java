package com.boarsoft.cache.demo.controller;

import com.boarsoft.cache.spring.api.LockService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.TimeUnit;

/**
 * @Description: 分布式锁测试controller
 * @Author: 刘超
 * @Date: 2023-07-19 10:02
 */
@RestController
@RequestMapping("/lock")
public class LockController {

    private static final Logger logger = LoggerFactory.getLogger(LockController.class);
    @Autowired
    LockService lockService;

    @RequestMapping("/test")
    public String test() {
        //尝试获取锁，key为“test”,value为线程名称
        Boolean lock = lockService.lock("test", Thread.currentThread().getName());
        logger.info("try lock result: " + lock);

        if (lock) {
            try {
                //打印获取锁线程名称
                System.out.println(Thread.currentThread().getName() + " get lock");
                //模拟业务处理耗时
                TimeUnit.SECONDS.sleep(5);
                return "ok";
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            } finally {
                //释放锁
                Boolean unlock = lockService.unlock("test", Thread.currentThread().getName());
                logger.info("try unlock result: " + unlock);
                //失败的话可以在此处做处理
                if (!unlock) {
                    //异常处理机制
                }
            }
        }

        return Thread.currentThread().getName() + " fail";
    }

    @RequestMapping("/test1")
    public String test1() {
        //尝试获取锁，key为“test”,value为线程名称
        Boolean lock = lockService.lock("test", Thread.currentThread().getName());

        logger.info("try lock result: " + lock);

        if (lock) {
            try {
                //打印获取锁线程名称
                System.out.println(Thread.currentThread().getName() + " get lock");
                //模拟业务处理耗时
                TimeUnit.SECONDS.sleep(5);
                return "ok";
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            } finally {
                //释放锁
                Boolean unlock = lockService.unlock("test", Thread.currentThread().getName());
                logger.info("try unlock result: " + unlock);
                //失败的话可以在此处做处理
                if (!unlock) {
                    //异常处理机制
                }
            }
        }

        return Thread.currentThread().getName() + " fail";
    }
}
