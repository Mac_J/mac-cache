package com.boarsoft.cache.demo.config;

import com.google.common.hash.BloomFilter;
import com.google.common.hash.Funnels;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.nio.charset.Charset;

/**
 * @Description: 布隆过滤器配置
 * @Author: 刘超
 * @Date: 2023-07-20 9:20
 */
@Configuration
public class BloomFilterConfig {
    /**
     * expectedInsertions：期望添加的数据个数
     * fpp：期望的误判率，期望的误判率越低，布隆过滤器计算时间越长
     * @return
     */
    @Bean
    public BloomFilter<String> testModelBloom(){
        BloomFilter<String> filter = BloomFilter.create(Funnels.stringFunnel(Charset.forName("utf-8")), 1000,0.00001);
        return filter;
    }
}
