package com.boarsoft.cache.demo.model;

import java.io.Serializable;

/**
 * @Description: 缓存测试Model
 * @Author: 刘超
 * @Date: 2023-03-01 17:43
 */
public class TestModel implements Serializable {

    private static final long serialVersionUID = 8306047660846439316L;
    private String msg;

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
