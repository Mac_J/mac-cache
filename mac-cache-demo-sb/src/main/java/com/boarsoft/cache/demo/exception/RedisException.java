package com.boarsoft.cache.demo.exception;

/**
 * @Description: redis异常
 * @Author: 刘超
 * @Date: 2023-07-19 19:46
 */
public class RedisException extends AbstractBaseException{

    private static final boolean CAN_RETRY = false;

    public RedisException(String errorCode){
        super(errorCode,CAN_RETRY);
    }

    public RedisException(String errorCode, Object... placeHolder){
        super(errorCode,CAN_RETRY,placeHolder);
    }

}
