package com.boarsoft.cache.demo.service;


import com.boarsoft.cache.demo.model.TestModel;

import javax.servlet.http.HttpSession;

/**
 * @Description:
 * @Author: 刘超
 * @Date: 2023-03-01 9:57
 */
public interface CacheService {

    TestModel getData(int id);

    String getSession(int id, HttpSession session);
}
