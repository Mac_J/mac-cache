package com.boarsoft.cache.demo.exception;

/**
 * @Description:
 * @Author: LiuChao
 * @CreateTime: 2022/5/11 10:02
 */
public abstract class AbstractBaseException extends RuntimeException{
    private String resultCode;
    private boolean canRetry = false;
    public AbstractBaseException(String resultCode, boolean canRetry, Object ... placeHolder) {
        super (MessageUtil.getChineseMessage(resultCode, placeHolder)) ;
        this.resultCode = resultCode;
        this.canRetry = canRetry;
    }
    public AbstractBaseException(String resultCode, boolean canRetry, Throwable cause, Object... placeHolder)
    {
        super (MessageUtil.getChineseMessage(resultCode,placeHolder),cause);
        this.resultCode = resultCode;
        this.canRetry = canRetry;
    }
    public String getMessage() { return super.getMessage(); }
    public String getResultCode() { return this.resultCode; }
    public void setResultCode (String resultCode) { this.resultCode = resultCode; }
    public boolean isCanRetry() { return this.canRetry; }
    public void setCanRetry (boolean canRetry) { this.canRetry = canRetry; }
}
