package com.boarsoft.cache.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.ImportResource;

/**
 * @Description:
 * @Author: 刘超
 * @Date: 2023-07-18 17:03
 */
@SpringBootApplication
@ImportResource({"classpath:conf/context.xml","classpath:application/*.xml"})
public class ApplicationStarter {
    public static void main(String[] args) {
        ConfigurableApplicationContext ctx = SpringApplication.run(ApplicationStarter.class, args);
        System.out.println("Startup ".concat(ctx.isRunning() ? "successfully." : "failed."));
    }
}
