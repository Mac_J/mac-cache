package com.boarsoft.cache.demo.exception;

import org.springframework.context.MessageSource;
import org.springframework.context.support.ResourceBundleMessageSource;

import java.text.MessageFormat;
import java.util.Locale;

public class MessageUtil {
    private static MessageSource messageSource = null;
    private static final String ENGLISHDEFAULTVALUE = "don't have error message config";
    private static final String CHINESEDEFAULTVALUE = "未配置错误信息";

    public MessageUtil() {}

    public static String getEnglishMessage (String code, Object[]param) {
        return messageSource.getMessage(code, param, "don't have error message config", Locale.US);
    }

    public static String getEnglishMessageNoParam (String code){
        return messageSource.getMessage(code, (Object[]) null, "don't have error message config", Locale.US);
    }
    public static String getChineseMessageNoParam (String code){
        return messageSource.getMessage(code, (Object[]) null,"o",Locale.SIMPLIFIED_CHINESE);
    }
    public static String getChineseMessage (String code, Object[]param){
        return messageSource.getMessage(code, param,"未配置错误信息",Locale.SIMPLIFIED_CHINESE);
    }
    public static String formatMessage (String message, Object...params){
        return message != null && params != null && params.length != 0 ? MessageFormat.format(message, params) : message;
    }
    static {
        messageSource = new ResourceBundleMessageSource();
        ((ResourceBundleMessageSource) messageSource).setBasenames(new String[]{"config/errorcode"});
        ((ResourceBundleMessageSource) messageSource).setDefaultEncoding("UTF-8");
    }
        ;



}


