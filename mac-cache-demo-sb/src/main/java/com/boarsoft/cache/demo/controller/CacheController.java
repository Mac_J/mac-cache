package com.boarsoft.cache.demo.controller;

import com.alibaba.fastjson.JSONObject;
import com.boarsoft.cache.demo.exception.RedisException;
import com.boarsoft.cache.demo.model.TestModel;
import com.boarsoft.cache.demo.service.CacheService;
import com.google.common.hash.BloomFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.redis.cache.RedisCacheConfiguration;
import org.springframework.data.redis.cache.RedisCacheManager;
import org.springframework.data.redis.cache.RedisCacheWriter;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;
import org.springframework.web.bind.annotation.*;
import redis.clients.jedis.exceptions.JedisConnectionException;
import sun.util.resources.LocaleData;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.*;

/**
 * @Description: redis缓存测试controller
 * @Author: 刘超
 * @Date: 2023-03-01 9:55
 */
@RestController
@RequestMapping("/cache")
public class CacheController {

    @Autowired
    CacheService cacheService;

    @Autowired
    @Qualifier("testModelBloom")
    BloomFilter<String> testModelBloom;


    @Autowired
    JedisConnectionFactory jedisConnectionFactory;

    @Autowired
    RedisCacheManager redisCacheManager;

    private Map<String, RedisCacheConfiguration> initialCacheConfigurations = new HashMap<>();
    private boolean allowInFlightCacheCreation;

    private static Logger logger = LoggerFactory.getLogger(CacheController.class);


    @GetMapping("/cacheTest1")
    public String cacheTest1(@RequestParam(name = "id", required = false) int id) {
        return cacheService.getData(id).getMsg() + " at " + LocalDateTime.now();

    }

    @GetMapping("/cacheTest2/{id}")
    public String cacheTest2(@PathVariable(name = "id", required = false) int id) {
        return cacheService.getData(id).getMsg();
    }

    @GetMapping("/bloom")
    public String bloom(@RequestParam(name = "id", required = false) int id) {
        if (testModelBloom.mightContain(String.valueOf(id))) {
            logger.info("布隆过滤器包含key: " + id);
            return "bloom contain key " + id;
        } else {
            logger.info("布隆过滤器不包含key: " + id);
            return "bloom has no key like " + id;
        }
    }

    @GetMapping("/switch")
    public String change(){
        jedisConnectionFactory.setHostName("127.0.0.1");
        jedisConnectionFactory.setPort(6380);
        jedisConnectionFactory.afterPropertiesSet();
        RedisCacheWriter cacheWriter = //
                RedisCacheWriter.nonLockingRedisCacheWriter(jedisConnectionFactory);
        redisCacheManager = new RedisCacheManager(cacheWriter, //
                RedisCacheConfiguration.defaultCacheConfig(), //
                initialCacheConfigurations, allowInFlightCacheCreation);
        redisCacheManager.initializeCaches();
        return "ok at " + LocalDateTime.now();
    }

    @GetMapping("/session")
    public String session(@RequestParam(name = "id", required = false) int id,HttpServletRequest request){
        HttpSession session = request.getSession();
        return cacheService.getSession(id,session);
    }

    @GetMapping("/synch")
    public String synch(@RequestParam(name = "id", required = false) int id) throws ExecutionException, InterruptedException {
        ExecutorService executor = Executors.newFixedThreadPool(1);
        Future<String> future = executor.submit(new Callable<String>() {
            @Override
            public String call() throws Exception {

                System.out.println("===task start===");
                TestModel data = cacheService.getData(id);
                Thread.sleep(5000);
                System.out.println("===task finish===");
                return JSONObject.toJSONString(data);
            }
        });
        //这里需要返回值时会阻塞主线程，如果不需要返回值使用是OK的。倒也还能接收
        String result=future.get();
        return result + " at " + LocalDateTime.now();
    }
}
