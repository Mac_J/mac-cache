package com.boarsoft.cache.demo.exception;

/**
 * @Description:
 * @Author: LiuChao
 * @CreateTime: 2022/5/11 9:51
 */
public class BizException extends AbstractBaseException{

    private static final boolean CAN_RETRY = false;

    public BizException(String errorCode){
        super(errorCode,CAN_RETRY);
    }

    public BizException(String errorCode, Object... placeHolder){
        super(errorCode,CAN_RETRY,placeHolder);
    }

}
